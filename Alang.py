import Lexer
import AST
import Parser
import CodeGen
import Evaluator
import Debug
import CParse

import llvmlite.binding as llvm # type: ignore

import os
import typing
import time
import argparse
import sys

__version__ = "0.0.1"
#c types
#cimport
#uint
#trenary operator
#sizeof

#finally implement inline

#enum types (enum identifier handling needed for errors probably too)
#union types
#proposed error handling needs unions

#above enough for std lib & self hosting & package manager & build system?

#after
	#generics/metaprogramming

	#thread local llvmlite cant do, has to go into self hosted first

	#implicit cast like in wiw.als

	#oninit

includePaths: typing.List[str] = []

if __name__ == '__main__':
	start_time = time.perf_counter()
	argParser = argparse.ArgumentParser(prog='alang', description='Without input files you get a REPL')
	argParser.add_argument('files', type=argparse.FileType('r'), nargs='*')
	argParser.add_argument('-I', '--Imports', nargs='*')
	argParser.add_argument('-l', action='append', nargs='*')
	argParser.add_argument('-o', '--output-file')
	argParser.add_argument('-O', type=int, choices=range(0, 4), default=0)
	argParser.add_argument('--emit-llvm', action='store_true')
	argParser.add_argument('--parse-c', action='store_true')
	argParser.add_argument('-S', action='store_true')
	argParser.add_argument('-g', action='store_true')
	argParser.add_argument('--transpile-c', action='store_true')
	argParser.add_argument('--ast-dump', action='store_true')
	argParser.add_argument('-v', '--version', action='version', version="%(prog)s ("+__version__+")")
	args = vars(argParser.parse_args())
	args['l'] = args['l'] if args['l'] else []
	importpaths = ['./'] #@TODO add some default library paths too
	if(args['Imports']):
		importpaths += args['Imports']
	kalei = Evaluator.Evaluator(importpaths, args['l'])

	if(not args['files']):
		line = ""
		try:
			line = input(">> ")
		except EOFError:
			line = ""
		while(line != "quit"):
		#while True:
			try:
				node = kalei.parser.parse_toplevel(line)
				print(node)
				print(kalei.evaluate(node))
			except Exception as e:
				print(e)
				#break
			try:
				line = input(">> ")
			except EOFError:
				break

	else:
		if os.name == 'nt': #windows
			ccompiler = "cl"
		else: #unix
			ccompiler = "cc"

		output = "-o {0}".format(args['output_file']) if args['output_file'] else ""
		linkflags = ' '.join(list(map(lambda arg: '-l' + arg, [''.join(x) for x in args['l']])))

		'''if args["output_file"]:
			outputfilename = args["output_file"]
			if os.name == 'nt': #windows
				os.system("cl {0} -o {1}".format(filename, outputfilename))
			else: #unix
				os.system("cc -g {0} -o {1}".format(filename, outputfilename))
		else:
			if os.name == 'nt': #windows
				os.system("cl {0}".format(filename))
			else: #unix
				os.system("cc -g {0}".format(filename))'''

		if(args['parse_c']):
			#if len(sys.argv) > 1:
			#	filename  = sys.argv[1]

			for inputFilePath in args['files']:
				c = CParse.CParser()
				c.Parse(inputFilePath.name)

			exit()

		#if args['files']:
		for inputFilePath in args['files']:
			#try:
			if True:
				tmp = ""
				with open(inputFilePath.name, "r") as inputFile:
					tmp = inputFile.read()
					#kalei.evaluate(tmp, True, True)
					'''tmp = inputFile.readline()
					while tmp:
						print(tmp)
						kalei.evaluate(tmp, True, True)
						tmp = inputFile.readline()'''
				#kalei.codegen.module.name = inputFilePath.name
				if args['g']:
					kalei.codegen.DebugInfo = Debug.DebugInfo
					kalei.codegen.isdebug = True
					Debug.DebugInfo.is_optimized = args['O'] != 0
					Debug.DebugInfo.file(kalei.codegen.module, inputFilePath.name, os.path.abspath(inputFilePath.name).replace(inputFilePath.name, ""))
					Debug.DebugInfo.compile_unit(kalei.codegen.module)

				kalei.parser.give_buf(tmp, inputFilePath.name)
				kalei.parser.lexer.file = inputFilePath.name
				if(args['transpile_c']):
					obj = kalei.compile_to_c_code(tmp, inputFilePath.name)
				else:
					obj = kalei.compile_to_object_code(tmp, args['O'], args['emit_llvm'], args['ast_dump'], args['S'], inputFilePath.name)

				if args['S'] or args['emit_llvm']:
					assert(args["output_file"])
					filename = args["output_file"]
					with open(filename, 'w+') as obj_file:
						obj_file.write(obj)
				elif args['transpile_c']:
					file = open("alangtranspile.c", 'w+')
					file.write(obj)
					file.close()
					print("{0} {1} {2} {3}".format(ccompiler, "alangtranspile.c", output, linkflags))
					os.system("{0} {1} {2} {3}".format(ccompiler, "alangtranspile.c", output, linkflags))
					os.remove("alangtranspile.c")
				else:
					filename = str(inputFilePath.name).replace('.als', "") + '.o'
					with open(filename, 'wb+') as obj_file:
						obj_file.write(obj)
					os.system("{0} {1} {2} {3}".format(ccompiler, filename, output, linkflags))
					os.remove(filename)
			#except Exception as e:
			if False:
				print(e)
	llvm.shutdown()
	print("Whole compile took {0} seconds".format(time.perf_counter() - start_time))
