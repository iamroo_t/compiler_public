function squarecast2(x:int32) -> float {
	return cast(float, x * x);
}

function addf(x:float, y:float) -> float {
	return x + y;
}

function add3f(x:float, y:float, z:float) -> float {
	defer addf(x, z);
	addf(x, y); //expressions without effects will be elliminated
	return addf(x, addf(y, z));
}

//should get inlined to <2xfloat> for c interop
vec2:struct {
	x:float;
	y:double;
};

function test(a:vec2) -> float {
	b:float = 0.0f;
	a.x = b;
	b = a.x;

	return b;
}

function test2() -> vec2 {
	a:vec2;
	a.x = cast(float, 5.0);
	a.y = cast(double, 7.0f);
	return a;
}

function degenerateadd(x:float, y:float) -> float {
	x = y; //params can be mutated too
	return x + y;
}

function squaref(x:float) -> float {
	y:float = x;
	y + 1.0f; //dead code
	return y * y;
}

function squarei(z:int32) -> int32 {
	k:int32 = z;
	k + 1;
	return k * k;
}

function squarecast(x:float) -> int32 {
	return cast(int32, x * x);
}

vec3:struct {
	x:float;
	y:float;
	z:float;
};
