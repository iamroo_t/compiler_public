function binary ! (a:int32, b:int32) -> int32 {
	return a - b;
}

/*function binary ((a:int32, b:int32) -> int32 {
	return 0 - a;
}

function unary ((a:int32) -> int32 {
	return 0 - a;
}

function unary )(a:int32) -> int32 {
	return 0 - a;
}*/

function unary ! (a:int32) -> int32 {
	return 0 - a;
}
