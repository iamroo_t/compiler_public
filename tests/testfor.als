//should return n
function testfor(n:int32) -> int32 {
	a:int32 = 0;

	for(i:int32; i < n; i = i + 1) {
		a = i;
	}
	return a;
}

function testinfiniteloop() -> int32 {
	a:int32;

	for(; a < 0;) {
		a = a + 1;
	}

	for(;;a = a + 1) {
		a = a + 1;
	}

	for(b:int32;;) {
		a = b + 1;
	}

	for(;;) {
		a = a + 1;
	}

	return a; //won't return
}
