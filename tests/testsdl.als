external function SDL_Init(a:int32) -> int32;
external function SDL_GetError() -> char ptr;
external function SDL_CreateWindow(a:char ptr, b:int32, c:int32, d:int32, e:int32, f:int32) -> char ptr;
external function SDL_GetWindowSurface(a:char ptr) -> char ptr;
external function SDL_FillRect(a:char ptr, b:int32, c:int32) -> any;
external function SDL_MapRGB(a:int32, r:int32, g:int32, b:int32) -> int32;
external function SDL_UpdateWindowSurface(a:char ptr) -> any;
external function SDL_Delay(a:int32) -> any;
external function SDL_DestroyWindow(a:char ptr) -> any;
external function SDL_Quit() -> any;

external function printf(fmt:immutable char ptr, va:vaargs) -> int32;

implicitcast(printf, 0, char[], arg) {
	cast(char ptr, addressof(arg[0]));
}

SDL_Surface:struct {tmp:int64;};

function main() -> int32 {
	if(SDL_Init(32/*SDL_INIT_VIDEO*/) < 0) {
		printf("could not initialize sdl2: %s\n", SDL_GetError());
		return 1;
	}
	window:char ptr = SDL_CreateWindow(cast(char ptr, "hello_sdl2"), 0/*SDL_WINDOWPOS_UNDEFINED*/, 0/*SDL_WINDOWPOS_UNDEFINED*/, 640, 480, 0/*SDL_WINDOW_SHOWN*/);

	/*if(window == NULL) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}*/
	//screenSurface:char/*SDL_Surface*/ ptr = SDL_GetWindowSurface(window);
	// badtmp:any = screenSurface.format;
	/*tmp:int32 = SDL_MapRGB(0, 255, 255, 255);
	SDL_FillRect(screenSurface, 0, tmp);*/
	SDL_UpdateWindowSurface(window);
	SDL_Delay(2000);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
