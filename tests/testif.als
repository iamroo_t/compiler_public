function testif(x:int32) -> int32 {
	a:float = cast(float, x);
	if(x) {
		if(a < 2.0f) {
			return cast(int32, 1.0f);
		}
		else if(a < 3.0f) {
			return 2;
		}
		else if(a < 4.0f) {
			return cast(int32, 3.0);
		}
	}
	else {
		a = 0.0f;
	}
	return cast(int32, a);
}
