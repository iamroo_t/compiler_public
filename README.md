What is this?
-------------

LLVM based reference implementation compiler for a language I've been working on

Dependencies
--------------------
The only dependency is [numba's llvmlite](https://github.com/numba/llvmlite/). and a python implementation with [PEP-484](https://www.python.org/dev/peps/pep-0484/) support(ie:python 3.5 and onwards)

How to use
--------------------
Run `python Alang.py` for a REPL or `python Alang.py [list] [of] [files]` to compile them to object code
Run `python Alang.py --help` for more information

Example usage
--------------------
For a language reference [go here](CODING.md)

In the working directory of the repository run ```python Alang.py test.als -I std test ./ -O0 -o test``` then run the output by running ```./test```

As a "better C"
--------------------
The language improves upon C by:
* getting rid of implicit conversions completly(except for the **any** type)
* adding deferred expressions, without the need of runt time support
* making functions first class and adding anonymous functions
* performes tail call optimization even at this early stage with optimizations turned off(even without LLVM's mem2rreg pass)

Why write the compiler in python then?
--------------------
Mainly for fast iteration, for the initial release I'm planning to self host the compiler anyway

This reference implementation as of 2020.Jan.13 has been written in 2 weeks
