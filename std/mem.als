external function malloc(size:int64) -> any ptr;
external function realloc(prev:any ptr, size:int64) -> any ptr;
external function free(tofree:any ptr) -> none;

function stdmalloc(size:int64) -> any ptr {
	ret:any ptr = malloc(size);

	/*if(ret == 0) {
		//error
	}*/

	return ret;
}

function stdrealloc(prev:any ptr, size:int64) -> any ptr {
	prevtmp:any ptr = prev;

	ret:any ptr = realloc(prev, size);

	/*if(ret == 0) {
		//error
		prev = prevtmp;
	}*/

	return ret;
}

function stdfree(tofree:any ptr) -> none {
	free(tofree);
	return;
}
