string:struct {
	m:char ptr;
	len:int32;
};

external function printf(fmt:immutable char ptr, va:vaargs) -> int32;

global print:function (fmt:immutable char ptr, va:vaargs) -> int32 = printf;
//global a:int32;
/*function print(fmt:immutable char ptr, va:vaargs) -> int32 {
	//return printf(fmt, va);
	return 1;
}*/

stdt:struct {
	print:function (fmt:immutable char ptr, va:vaargs) -> int32;
	//printarr:function (fmt:immutable char ptr, va:vaargs) -> int32; how should function arrays be declared?
};

//more like this with some kind of initializer list
//global std:stdt = printf;
