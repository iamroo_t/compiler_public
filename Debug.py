from llvmlite.ir import DIToken, Constant, IntType # type: ignore

class SourceLocation(object):
	def __init__(self, line, column, file):
		self.column = column
		self.line = line
		self.file = file

class DummyDebugInfo(object):
	current_scope = None
	dbgDeclareFunc = None
	dbgValueFunc = None
	dbgAddrFunc = None

	def function_type(module, proto):
		pass

	def function(module, name, line, typee):
		pass

	def file(module, filename, directory):
		pass

	def expression(module):
		pass

	def variable_type(module, name, size):
		pass

	def lexicalblock(module):
		pass

	def local_variable(module, name, typee):
		pass

	def location(module):
		pass

	def compile_unit(module):
		pass

	def dwarf_and_debug_info(module):
		pass

class DebugInfo(object):
	debugfile = ""
	sourceLocation = SourceLocation(0, 0, "")
	unit = None
	current_scope = None
	is_optimized = False
	doubletype = None
	dbgDeclareFunc = None
	dbgValueFunc = None
	dbgAddrFunc = None

	def function_type(module, proto):
		return module.add_debug_info("DISubroutineType", {
			"types": module.add_metadata([None])#proto]),#[None]
		})

	def function(module, name, line, typee):
		r = module.add_debug_info("DISubprogram", {
			"name": name,
			"file": DebugInfo.debugfile,
			"line": DebugInfo.sourceLocation.line,
			"type": typee,
			"scope": DebugInfo.unit,#DebugInfo.current_scope,
			"scopeLine": DebugInfo.sourceLocation.line,
			"isLocal": False,
			"unit": DebugInfo.unit,
			"isDefinition": True
			}, is_distinct=True)
		DebugInfo.current_scope = r
		return r

	def file(module, filename, directory):
		DebugInfo.current_scope = DebugInfo.debugfile = module.add_debug_info("DIFile", {
			"filename": filename,
			"directory": directory,
		})

	def expression(module):
		return module.add_debug_info("DIExpression", {})

	#FIXME
	def variable_type(module, name, size):
		if DebugInfo.doubletype == None:
			#return 
			DebugInfo.doubletype = module.add_debug_info("DIBasicType", {
				"name": name,
				"size": size,
				"encoding": DIToken("DW_ATE_float")
			})
		return DebugInfo.doubletype

	def lexicalblock(module):
		DebugInfo.current_scope = module.add_debug_info("DILexicalBlock", {
			"scope": DebugInfo.current_scope,
			"file": DebugInfo.debugfile,
			"line": DebugInfo.sourceLocation.line,
			"column": DebugInfo.sourceLocation.column
		}, is_distinct=True)

	#FIXME
	def local_variable(module, name, typee):
		#if DebugInfo.current_scope == None:
		#	return
		return module.add_debug_info("DILocalVariable", {
			"name": name,
			"scope": DebugInfo.current_scope,
			"file": DebugInfo.debugfile,
			"line": DebugInfo.sourceLocation.line,
			"type": DebugInfo.doubletype#typee
		})

	def location(module):
		return module.add_debug_info("DILocation", {
			"line": DebugInfo.sourceLocation.line,
			"column": DebugInfo.sourceLocation.column,
			"scope": DebugInfo.current_scope
		})

	def compile_unit(module):
		DebugInfo.unit = module.add_debug_info("DICompileUnit", {
			"language":        DIToken("DW_LANG_C99"),
			"file":            DebugInfo.debugfile,
			"producer":        "ALANG compiler",
			"runtimeVersion":  0,
			"isOptimized":     DebugInfo.is_optimized,
			'emissionKind':    1,#FullDebug 0=NoDebug
			"enums":           [],
			"nameTableKind":   DIToken("None"),
		}, is_distinct=True)

	def dwarf_and_debug_info(module):
		module.add_named_metadata("llvm.dbg.cu", DebugInfo.unit)
		#refer to https://llvm.org/docs/LangRef.html#module-flags-metadata for the reason of the first Constans value(2 warning, 1 error, 7 max if modules disagree)
		module.add_named_metadata("llvm.module.flags", [Constant(IntType(32), 2), "Dwarf Version", Constant(IntType(32), 4)])
		module.add_named_metadata("llvm.module.flags", [Constant(IntType(32), 2), "Debug Info Version", Constant(IntType(32), 3)])
		module.add_named_metadata("llvm.module.flags", [Constant(IntType(32), 1), "wchar_size", Constant(IntType(32), 4)])
		module.add_named_metadata("llvm.module.flags", [Constant(IntType(32), 7), "PIC Level", Constant(IntType(32), 2)])
		module.add_named_metadata("llvm.module.flags", [Constant(IntType(32), 7), "PIE Level", Constant(IntType(32), 2)])
		module.add_named_metadata("llvm.ident", ["alang version 0.0.1"])
#!6 = !{i32 7, !"PIC Level", i32 2}
#!7 = !{i32 7, !"PIE Level", i32 2}
