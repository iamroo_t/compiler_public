from ErrWarnPrint import *
import AST
import clang.cindex
#import clang.enumerations

class CParser(object):
	def __init__(self):
		self.INDENT = 2
		self.K = clang.cindex.CursorKind
		self.visited = set()

	def is_std_ns(self, node):
		return node.kind == self.K.NAMESPACE and node.spelling == 'std'

	def Dump(self, node: clang.cindex.Cursor, indent: int, saw):
		if indent == 0:
			pre = ''
		elif indent < 4:
			pre = '|-'
		else:
			pre = '|' + ' ' * (indent - 3) + '`-'
		k = node.kind  # type: clang.cindex.CursorKind
		if not k.is_unexposed():
			print(pre, end='')
			print(k.name, end=' ')
			if node.spelling:
				print('s:', node.spelling, end=' ')
				if node.type.spelling:
					print('t:', node.type.spelling, end=' ')
			print()
		saw.add(node.hash)
		if node.referenced is not None and node.referenced.hash not in saw:
			self.Dump(node.referenced, indent + self.INDENT, saw)
		skip = len([c for c in node.get_children() if indent == 0 and self.is_std_ns(c)])
		for c in node.get_children():
			if not skip:
				self.Dump(c, indent + self.INDENT, saw)
			if indent == 0 and self.is_std_ns(c):
				skip -= 1
		try:
			saw.remove(node.hash)
		except:
			print()

	def _ASTGen(self, node, name):
		"""Node visitor. Dispathces upon node type.

		For AST node of class Foo, calls self._ASTGen_Foo. Each visitor is
		expected to return a AST.ASTNode.
		"""
		method = '_ASTGen_' + name
		return getattr(self, method)(node)

	#def _ASTGen_TRANSLATION_UNIT(self, node):
	#	pass

	def _ASTGen_UNEXPOSED_ATTR(self, node):
		pass

	def _ASTGen_UNEXPOSED_EXPR(self, node):
		pass

	#used from here

	def _ASTGen_FUNCTION_DECL(self, node):
		pass

	def _ASTGen_PARM_DECL(self, node):
		pass

	def _ASTGen_STRUCT_DECL(self, node):
		pass

	def _ASTGen_TYPE_REF(self, node):
		pass

	def _ASTGen_STRING_LITERAL(self, node):
		pass

	def _ASTGen_TYPEDEF_DECL(self, node):
		pass

	def _ASTGen_FIELD_DECL(self, node):
		pass

	def _ASTGen_UNION_DECL(self, node):
		pass

	def _ASTGen_VAR_DECL(self, node):
		pass







	def _ASTGen_COMPOUND_STMT(self, node):
		pass

	def _ASTGen_DECL_STMT(self, node):
		pass

	def _ASTGen_INTEGER_LITERAL(self, node):
		pass

	def _ASTGen_IF_STMT(self, node):
		pass

	def _ASTGen_DECL_REF_EXPR(self, node):
		pass

	def _ASTGen_RETURN_STMT(self, node):
		pass

	def _ASTGen_CALL_EXPR(self, node):
		pass

	def _ASTGen_UNARY_OPERATOR(self, node):
		pass

	def _ASTGen_FOR_STMT(self, node):
		pass

	def _ASTGen_BINARY_OPERATOR(self, node):
		pass

	def _ASTGen_ARRAY_SUBSCRIPT_EXPR(self, node):
		pass

	def _ASTGen_ASM_LABEL_ATTR(self, node):
		pass

	def _ASTGen_CONST_ATTR(self, node):
		pass

	def GenerateAST(self, node: clang.cindex.Cursor, ret = []):
		print(node.kind.name)
		#broken and recurses because node doesnt get into visited and/or check fails
		#if node.referenced is not None and node.referenced.hash not in self.visited:
		#	ret.append(self._ASTGen(node.referenced, node.referenced.kind.name))
		#	self.GenerateAST(node.referenced, ret)
		self.visited.add(node.hash)
		for c in node.get_children():
			ret.append(self._ASTGen(c, c.kind.name))
			self.GenerateAST(c, ret)
		try:
			self.visited.remove(node.hash)
		except:
			print() #do nothing
		return ret

	def NextNode(node: clang.cindex.Cursor):
		pass

	def Parse(self, filename: str):
		index = clang.cindex.Index.create()
		tu = index.parse(filename, ['-Wall', '-Wextra'])
		self.Dump(tu.cursor, 0, set())
		print(self.GenerateAST(tu.cursor))
