from enum import Enum
from collections import namedtuple
from Debug import SourceLocation
import typing

# Each token is a tuple of kind and value. kind is one of the enumeration values
# in TokenKind. value is the textual value of the token in the input.
class TokenKind(Enum):
	EOF = -1
	DEF = -2
	EXTERN = -3
	IDENTIFIER = -4
	TYPE = -5
	NUMBER = -6
	OPERATOR = -7
	IF = -8
	ELIF = -9
	ELSE = -10
	FOR = -11
	RETURN = -12
	RETURNTYPE = -13
	CURLYOPEN = -14
	CURLYCLOSE = -15
	SEMICOLON = -16
	DOUBLECOLON = -17
	STRUCT = -18
	BINARY = -19
	UNARY = -20
	NAMESPACE = -21
	IMPORT = -22
	AS = -23
	DEFER = -24
	PTR = -25
	STRING = -26
	CHAR = -27
	QUALIFIER = -28
	PARENOPEN = -29
	PARENCLOSE = -30
	INLINE = -31
	INTERNAL = -32
	GLOBAL = -33
	BRACKETOPEN = -34
	BRACKETCLOSE = -35
	TRUE = -36
	FALSE = -37
	SWITCH = -38
	CASE = -39
	ENUM = -40
	BREAK = -41
	CONTINUE = -42
	IMPLICITCAST = -43
	IGNORE = -44
	UNDEFINED = -45
	THROW = -46
	CATCH = -47
	TRY = -48
	THROWS = -49
	UNION = -50
	WHILE = -51
	DO = -52
	COMPTIME = -53

Token = namedtuple('Token', 'kind value')

class Lexer(object):
	"""
	Initialize the lexer with a string buffer. tokens() returns a generator that
	can be queried for tokens. The generator will emit an EOF token before
	stopping.
	"""
	def __init__(self, buf, f = "unknown"):
		assert len(buf) >= 1
		self.buf = buf
		self.pos = 0
		self.lastchar = self.buf[0]
		self.file = f
		self.loc = SourceLocation(1, 0, f)
		self.splitlines = buf.split('\n')

		self._qualifier_map = {
			'register':     TokenKind.QUALIFIER,
			'immutable':    TokenKind.QUALIFIER,
			'thread_local': TokenKind.QUALIFIER,
			'atomic':       TokenKind.QUALIFIER,
		}

		self._keyword_map = {
			'function':     TokenKind.DEF,
			'external':     TokenKind.EXTERN,
			'internal':     TokenKind.INTERNAL,
			'inline':       TokenKind.INLINE,
			'if':           TokenKind.IF,
			'elif':         TokenKind.ELIF,
			'else':         TokenKind.ELSE,
			'for':          TokenKind.FOR,
			'return':       TokenKind.RETURN,
			'namespace':    TokenKind.NAMESPACE,
			'import':       TokenKind.IMPORT,
			'as':           TokenKind.AS,
			'defer':        TokenKind.DEFER,
			'struct':       TokenKind.STRUCT,
			'binary':       TokenKind.BINARY,
			'unary':        TokenKind.UNARY,
			'ptr':          TokenKind.PTR,
			'global':       TokenKind.GLOBAL,
			'true':         TokenKind.TRUE,
			'false':        TokenKind.FALSE,
			'switch':       TokenKind.SWITCH,
			'case':         TokenKind.CASE,
			'enum':         TokenKind.ENUM,
			'break':        TokenKind.BREAK,
			'continue':     TokenKind.CONTINUE,
			'implicitcast': TokenKind.IMPLICITCAST,
			'ignore':       TokenKind.IGNORE,
			'undefined':    TokenKind.UNDEFINED,
			'try':          TokenKind.TRY,
			'catch':        TokenKind.CATCH,
			'throw':        TokenKind.THROW,
			'throws':       TokenKind.THROWS,
			'union':        TokenKind.UNION,
			'while':        TokenKind.WHILE,
			'do':           TokenKind.DO,
			'comptime':     TokenKind.COMPTIME,
		}

		self._token_map = {
			TokenKind.DEF:         'function',
			TokenKind.EXTERN:      'extern',
			TokenKind.INTERNAL:    'internal',
			TokenKind.INLINE:      'inline',
			TokenKind.IF:          'if',
			TokenKind.ELIF:        'elif',
			TokenKind.ELSE:        'else',
			TokenKind.FOR:         'for',
			TokenKind.RETURN:      'return',
			TokenKind.RETURNTYPE:  '->',
			TokenKind.CURLYOPEN:   '{',
			TokenKind.CURLYCLOSE:  '}',
			TokenKind.PARENOPEN:   '(',
			TokenKind.PARENCLOSE:  ')',
			TokenKind.SEMICOLON:   ';',
			TokenKind.DOUBLECOLON: ':',
			TokenKind.STRUCT:      'struct',
			TokenKind.BINARY:      'binary',
			TokenKind.UNARY:       'unary',
			TokenKind.OPERATOR:    'operator',
			TokenKind.NUMBER:      'number',
			TokenKind.STRING:      'string',
			TokenKind.PTR:         'ptr',
			TokenKind.GLOBAL:      'global',
			TokenKind.BRACKETOPEN: '[',
			TokenKind.BRACKETCLOSE:']',
			TokenKind.TRUE:        'true',
			TokenKind.FALSE:       'false',
			TokenKind.SWITCH:      'switch',
			TokenKind.CASE:        'case',
			TokenKind.ENUM:        'enum',
			TokenKind.BREAK:       'break',
			TokenKind.IMPLICITCAST:'implicitcast',
			TokenKind.DEFER:       'defer',
			TokenKind.IGNORE:      'ignore',
			TokenKind.UNDEFINED:   'undefined',
			TokenKind.IMPORT:      'import',
			TokenKind.THROWS:      'throws',
			TokenKind.THROW:       'throw',
			TokenKind.TRY:         'try',
			TokenKind.CATCH:       'catch',
			TokenKind.UNION:       'union',
			TokenKind.WHILE:       'while',
			TokenKind.DO:          'do',
			TokenKind.COMPTIME:    'comptime',
		}

	def map_token(self, token_kind):
		if token_kind != TokenKind.IDENTIFIER:
			return self._token_map[token_kind]
		else:
			return "identifier"

	def get_current(self):
		return str(self.file + ':' + str(self.loc.line) + '\n' + self.splitlines[self.loc.line - 1] + '\n' + ' ' * self.loc.column + '^\n')

	def location(self):
		return SourceLocation(self.loc.line, self.loc.column, self.loc.file)

	def tokens(self) -> typing.Generator[Token, None, None]:
		nest = 0
		while self.lastchar:
			# Skip whitespace
			while self.lastchar.isspace():
				if self.lastchar == '\n' or self.lastchar == '\r' or self.lastchar == '\r\n':
					self.loc.line += 1
					self.loc.column = 0
				self._advance()
			if self.lastchar == "\"":
				strr = ""
				self._advance()
				sawbackslash = False
				while (sawbackslash and self.lastchar == "\"") or self.lastchar != "\"":
					if self.lastchar == '\\':
						sawbackslash = True
					else:
						sawbackslash = False
					strr += self.lastchar
					self._advance()
				self._advance()
				strr = strr.replace('\\n', '\n')
				strr = strr.replace('\\"', '"')
				yield Token(kind=TokenKind.STRING, value=strr)

			# Identifier, qualifier or keyword
			elif self.lastchar.isalpha() or self.lastchar == '_':
				id_str = ''
				#type_str = ''
				return_type = ''
				while self.lastchar.isalnum() or self.lastchar == '_':
					id_str += self.lastchar
					self._advance()
				if id_str in self._keyword_map:
					yield Token(kind=self._keyword_map[id_str], value=id_str)
				elif id_str in self._qualifier_map:
					yield Token(kind=TokenKind.QUALIFIER, value=id_str)
				else:
					yield Token(kind=TokenKind.IDENTIFIER, value=id_str)

			# Number
			elif self.lastchar.isdigit() or self.lastchar == '.':
				num_str = ''
				while self.lastchar.isdigit() or self.lastchar == '.' or self.lastchar == 'f':
					num_str += self.lastchar
					self._advance()
				yield Token(kind=TokenKind.NUMBER, value=num_str)

			# Single line comment
			elif self._get_next_two() == '//':
				self._advance()
				while self.lastchar and self.lastchar not in '\r\n':
					self._advance()

			# Multi line comment
			elif self._get_next_two() == '/*':
				self._advance()
				while self.lastchar and self._get_next_two() != '*/':
					if self.lastchar == '\n' or self.lastchar == '\r' or self.lastchar == '\r\n':
						self.loc.line += 1
						self.loc.column = 0
					self._advance()
				self._advance()
				self._advance()

			elif self.lastchar:
				# Some other char
				if self._get_next_two() == "->":
					self._advance()
					self._advance()
					yield Token(kind=TokenKind.RETURNTYPE, value='return_type')
					#self._advance() #do not consume the next token after the type

				elif self.lastchar == '{':
					nest += 1
					yield Token(kind=TokenKind.CURLYOPEN, value = nest)
					self._advance()

				elif self.lastchar == '}':
					nest -= 1
					yield Token(kind=TokenKind.CURLYCLOSE, value = nest)
					self._advance()

				elif self.lastchar == '[':
					nest += 1
					yield Token(kind=TokenKind.BRACKETOPEN, value = "")
					self._advance()

				elif self.lastchar == ']':
					nest -= 1
					yield Token(kind=TokenKind.BRACKETCLOSE, value = "")
					self._advance()

				elif self.lastchar == ';':
					yield Token(kind=TokenKind.SEMICOLON, value = ';')
					self._advance()

				elif self.lastchar == ':':
					yield Token(kind=TokenKind.DOUBLECOLON, value = ':')
					self._advance()

				elif self.lastchar == '(':
					yield Token(kind=TokenKind.PARENOPEN, value = '(')
					self._advance()

				elif self.lastchar == ')':
					yield Token(kind=TokenKind.PARENCLOSE, value = ')')
					self._advance()

				else:
					operator = self.lastchar
					self._advance()
					while (not self.lastchar.isalnum()) and (not self.lastchar.isspace()) and self.lastchar not in ['{', '}', ';', ':', ')', '(']:
						operator += self.lastchar
						self._advance()
					yield Token(kind=TokenKind.OPERATOR, value=operator)
					#self._advance()

		yield Token(kind=TokenKind.EOF, value='')

	def _advance(self) -> None:
		try:
			self.pos += 1
			'''if self.lastchar == '\t':
				self.loc.column += 7
			else:'''
			self.loc.column += 1
			self.lastchar = self.buf[self.pos]
		except IndexError:
			self.lastchar = ''

	def _get_next_two(self) -> str:
		try:
			return str(self.buf[self.pos] + self.buf[self.pos+1])
		except Exception:
			return self.lastchar
