from Debug import SourceLocation

"""class TypeAST():
	def __init__(self, type, indirection, qualifiers):
		self.type = type
		self.indirection = indirection
		self.qualifiers = qualifiers

	def dump(self, indent=0):
		r = ""
		t = ""
		for i in self.qualifiers:
			r += i
		try:
			t = self.type.dump()
		except:
			t = self.type
		return '{0}{1}[{2} {3}{4}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, r, t, "*" * self.indirection)"""

# AST hierarchy
class ASTNode(object):
	#FIXME remove, rn needed for imports
	sourceLocation = SourceLocation(1, 0, "")
	tctype = None
	def dump(self, indent=0):
		raise NotImplementedError


class ExprAST(ASTNode):
	pass

class ComptimeExprAST(ASTNode):
	def __init__(self, body):
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}:{3}\n{2}'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.body.dump(indent + 2), self.irtype)

class UndefinedValueAST(ASTNode):
	def __init__(self):
		pass

	def dump(self, indent=0):
		return '{0}{1}'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__)

class ImportAST(ASTNode):
	def __init__(self, name, ast):
		self.name = name
		self.ast = ast

	def dump(self, indent=0):
		r = ""
		for i in self.ast:
			r += i.dump(indent + 2) + '\n'
		return '{0}[{1}]\n{2}{3}'.format(
			self.__class__.__name__, self.name, ' ' * (indent - 2) + '|-', r)

class NamespaceAST(ASTNode):
	def __init__(self, name):
		self.name = name

	def dump(self, indent=0):
		return '{0}{1}[{2}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.name)


class StringConstantAST(ASTNode):
	def __init__(self, val):
		self.val = val

	def dump(self, indent=0):
		return '{0}[{1}] [TYPE:{2}]'.format(
			self.__class__.__name__, self.val, self.irtype)


class NumberExprAST(ExprAST):
	def __init__(self, val, type):
		self.val = val
		self.type = type

	def dump(self, indent=0):
		return '{0}{1}[{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.val, self.irtype)


class BooleanExprAST(ExprAST):
	def __init__(self, val):
		self.val = val

	def dump(self, indent=0):
		return '{0}{1}[{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.val, self.irtype)


class BreakExprAST(ExprAST):
	def __init__(self):
		pass

	def dump(self, indent=0):
		return '{0}{1} [TYPE:{2}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.irtype)


class ContinueExprAST(ExprAST):
	def __init__(self):
		pass

	def dump(self, indent=0):
		return '{0}{1} [TYPE:{2}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.irtype)


class ImplicitCastExpr(ExprAST):
	def __init__(self, function, argcount, id_type, argname, body):
		self.function = function
		self.argcount = argcount
		self.id_type = id_type
		self.argname = argname
		self.body = body

	def dump(self, indent=0):
		return '{0}{1} function {2} argument {3} from type {4} by doing:\n{5}'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.function, self.argcount, self.id_type.dump(), self.body.dump(indent + 2))


class IntrinsicExprAST(ExprAST):
	def __init__(self, intrinsic, exprs):
		self.intrinsic = intrinsic
		self.exprs = exprs

	def dump(self, indent=0):
		r = ""
		for i in self.exprs:
			r += i.dump()
		return '{0}{1}[{2}({3})]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.intrinsic, r)


class AddressofExprAST(ExprAST):
	def __init__(self, expr):
		self.expr = expr

	def dump(self, indent=0):
		return '{0}{1}[{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.expr.dump(indent + 2), self.irtype)


class TypeofExprAST(ExprAST):
	def __init__(self, expr):
		self.expr = expr

	def dump(self, indent=0):
		return '{0}{1}[{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.expr.dump(indent + 2), self.irtype)


class DereferenceExprAST(ExprAST):
	def __init__(self, expr):
		self.expr = expr

	def dump(self, indent=0):
		return '{0}{1}[{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.expr.dump(indent + 2), self.irtype)


class ArrayAccessExprAST(ExprAST):
	def __init__(self, variable, element):
		self.variable = variable
		self.element = element

	def dump(self, indent=0):
		return '{0}{1}[{2}({3})]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.variable.dump(), self.element)


class CastExprAST(ExprAST):
	def __init__(self, type, expr):
		self.type = type
		self.expr = expr

	def dump(self, indent=0):
		return '{0}{1}[{2} to {3}] [TYPE:{4}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.expr.dump(0), self.type.dump(0), self.irtype)


#reference to (global) variable eg x
class VariableExprAST(ExprAST):
	def __init__(self, name):
		self.name = name

	def dump(self, indent=0):
		return '{0}{1}[{2} [TYPE:{3}]]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.name, self.irtype)


#definition of a global variable eg x:float [= 5.0f];
class GlobalVarExprAST(ExprAST):
	def __init__(self, name, id_type, init, linkage):#, body):
		# vars is a sequence of (name, type, init) pairs
		self.name = name
		self.id_type = id_type
		self.init = init
		self.linkage = linkage

		#self.body = body

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)
		s += '{0}{1}:[TYPE:{2}]'.format(prefix, self.name, self.irtype)
		if self.init is None:
			s += '\n'
		else:
			s += '=\n' + self.init.dump(indent + 2) + '\n'
		return s


#definition of variable eg x:float [= 5.0f];
class VarExprAST(ExprAST):
	def __init__(self, name, id_type, init):#, body):
		# vars is a sequence of (name, type, init) pairs
		self.name = name
		self.id_type = id_type
		self.init = init
		#self.body = body

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)
		s += '{0}{1}:[TYPE:{2}]'.format(prefix, self.name, self.irtype)
		if self.init is None:
			s += '\n'
		else:
			s += '=\n' + self.init.dump(indent + 2) + '\n'
		return s


class DeferExprAST(ExprAST):
	def __init__(self, body):
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}:{3}\n{2}'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.body.dump(indent + 2), self.irtype)


class ReturnExprAST(ExprAST):
	def __init__(self, body):
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}[\n{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.body.dump(indent + 2), self.irtype)

class ThrowExprAST(ExprAST):
	def __init__(self, body):
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}[\n{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.body.dump(indent + 2), self.irtype)

class TryExprAST(ExprAST):
	def __init__(self, body):
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}[\n{2}] [TYPE:{3}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.body.dump(indent + 2), self.irtype)

class CatchExprAST(ExprAST):
	def __init__(self, val, body):
		self.val = val
		self.body = body

	def dump(self, indent=0):
		return '{0}{1}[\n{2}{3}] [TYPE:{4}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.val, self.body.dump(indent + 2), self.irtype)

class UnaryExprAST(ExprAST):
	def __init__(self, op, operand):
		self.op = op
		self.operand = operand

	def dump(self, indent=0):
		s = '{0}{1}[{2} [TYPE:{3}]]\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.op, self.irtype)
		s += self.operand.dump(indent + 2)
		return s


class BinaryExprAST(ExprAST):
	def __init__(self, op, lhs, rhs):
		self.op = op
		self.lhs = lhs
		self.rhs = rhs

	def dump(self, indent=0):
		s = '{0}{1}[{2}]\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.op)
		s += self.lhs.dump(indent + 2) + '\n'
		s += self.rhs.dump(indent + 2)
		return s + "{0}:{1}".format(self.sourceLocation.line, self.sourceLocation.column)


class NoopAST(ASTNode):
	def __init__(self):
		pass

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1} [TYPE:{2}]'.format(prefix, self.__class__.__name__, self.irtype)
		return s


class CompoundStatementAST(ExprAST):
	def __init__(self, statements):
		self.statements = statements

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)

		for i in self.statements:
			s += '{0}\n'.format(
				i.dump(indent + 2))
		return s

class IfExprAST(ExprAST):
	def __init__(self, cond_expr, then_expr, has_else, else_expr):
		self.cond_expr = cond_expr
		self.then_expr = then_expr
		self.has_else = has_else
		self.else_expr = else_expr

	def dump(self, indent=0):
		#prefix = '|' + '-' * (indent - 1)
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1} has else:{2}\n'.format(prefix, self.__class__.__name__, self.has_else)
		s += '{0}Condition:\n{1}\n'.format(
			prefix, self.cond_expr.dump(indent + 2))
		s += '{0}Then:\n{1}\n'.format(
			prefix, self.then_expr.dump(indent + 2))
		if self.has_else:
			s += '{0} Else:\n{1}\n'.format(
				prefix, self.else_expr.dump(indent + 2))
		return s


class ForExprAST(ExprAST):
	def __init__(self, start_expr, end_expr, step_expr, body):
		self.start_expr = start_expr
		self.end_expr = end_expr
		self.step_expr = step_expr
		self.body = body

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)
		s += '{0} Start:\n{1}\n'.format(
			prefix, self.start_expr.dump(indent + 2))
		s += '{0} End:\n{1}\n'.format(
			prefix, self.end_expr.dump(indent + 2))
		s += '{0} Step:\n{1}\n'.format(
			prefix, self.step_expr.dump(indent + 2))
		s += '{0} Body:\n{1}\n'.format(
			prefix, self.body.dump(indent + 2))
		return s

class WhileExprAST(ExprAST):
	def __init__(self, end_expr, body):
		self.end_expr = end_expr
		self.body = body

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)
		s += '{0} End:\n{1}\n'.format(
			prefix, self.end_expr.dump(indent + 2))
		s += '{0} Body:\n{1}\n'.format(
			prefix, self.body.dump(indent + 2))
		return s

class DoWhileExprAST(ExprAST):
	def __init__(self, end_expr, body):
		self.end_expr = end_expr
		self.body = body

	def dump(self, indent=0):
		prefix = ' ' * (indent - 2) + '|-'
		s = '{0}{1}\n'.format(prefix, self.__class__.__name__)
		s += '{0} End:\n{1}\n'.format(
			prefix, self.end_expr.dump(indent + 2))
		s += '{0} Body:\n{1}\n'.format(
			prefix, self.body.dump(indent + 2))
		return s

class CallExprAST(ExprAST):
	def __init__(self, callee, args, catch, inline = False):
		self.callee = callee
		self.args = args
		self.inline = inline
		self.catch = catch

	def dump(self, indent=0):
		s = '{0}{1}[inline:{2}][{3}][TYPE:{4}]\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.inline, self.callee, self.irtype)
		for arg in self.args:
			s += arg.dump(indent + 2) + '\n'
		return s[:-1]  # snip out trailing '\n'


class PrototypeAST(ASTNode):
	def __init__(self, name, argnames, argtypes, returntype, throws, isoperator=False, prec=0):
		self.name = name
		self.argnames = argnames
		self.argtypes = argtypes
		self.returntype = returntype
		self.throws = throws
		self.isoperator = isoperator
		self.prec = prec
		self.ispolymorphic = False
		self.isoverloaded = False

	def is_unary_op(self):
		return self.isoperator and len(self.argnames) == 1

	def is_binary_op(self):
		return self.isoperator and len(self.argnames) == 2

	def get_op_name(self):
		assert self.isoperator
		return self.name.replace("binary", "").replace(" ", "")

	def dump(self, indent=0):
		p = []
		for i in range(len(self.argnames)):
			p.append(str(self.argnames[i]) + ":" + str(self.argtypes[i].dump()))
		s = '{0}{1} {2}({3}) -> [return TYPE:{4}] [proto TYPE:{5}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.name,
			', '.join(p), self.returntype.dump(), self.irtype)
		if self.isoperator:
			s += '[operator {0} with prec={1}]'.format(self.get_op_name(), self.prec)
		return s


class FunctionAST(ASTNode):
	def __init__(self, proto, body, ret, linkage = False):
		self.proto = proto
		self.body = body
		self.ret = ret
		self.linkage = linkage

	_anonymous_function_counter = 0

	@classmethod
	def create_anonymous(klass, body, proto):
		"""Create an anonymous function to hold an expression."""
		klass._anonymous_function_counter += 1
		assert(not proto.isoperator)
		return klass(
			PrototypeAST('_anon{0}'.format(klass._anonymous_function_counter), proto.argnames, proto.argtypes, proto.returntype, proto.throws),
			body, proto.returntype)

	def is_anonymous(self):
		return self.proto.name.startswith('_anon')

	def dump(self, indent=0):
		s = '{0}{1}[linkage:{2}][{3}]\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.linkage, self.proto.dump())
		#for i in self.body:
		s += self.body.dump(indent + 2) + '\n'
		return s

class MemberExprAST(ExprAST):
	def __init__(self, var, fieldname):
		self.var = var
		self.fieldname = fieldname

	def dump(self, indent=0):
		return '{0}{1}\n{2}\n{3} [TYPE:{4}]'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__, self.var.dump(indent+2), ' ' * (indent) + '|-' + self.fieldname, self.irtype)

class StructAST(ASTNode):
	def __init__(self, name, fieldnames, fieldtypes):
		self.name = name
		self.fieldnames = fieldnames
		self.fieldtypes = fieldtypes

	def dump(self, indent=0):
		ret = '{0}{1}[\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__)
		for name, type in zip(self.fieldnames, self.fieldtypes):
			ret += '{0}{1}:{2}\n'.format(' ' * (indent + 2), str(name), type.dump())
		ret += "] [TYPE:{0}]".format(self.irtype)
		return ret

class UnionAST(ASTNode):
	def __init__(self, name, fieldnames, fieldtypes):
		self.name = name
		self.fieldnames = fieldnames
		self.fieldtypes = fieldtypes

	def dump(self, indent=0):
		ret = '{0}{1}[\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__)
		for name, type in zip(self.fieldnames, self.fieldtypes):
			ret += '{0}{1}:{2}\n'.format(' ' * (indent + 2), str(name), type.dump())
		ret += "] [TYPE:{0}]".format(self.irtype)
		return ret

class EnumAST(ASTNode):
	def __init__(self, name, fieldnames, fieldtypes):
		self.name = name
		self.fieldnames = fieldnames

	def dump(self, indent=0):
		ret = '{0}{1}[\n'.format(
			' ' * (indent - 2) + '|-', self.__class__.__name__)
		for name in self.fieldnames:
			ret += '{0}{1}\n'.format(' ' * (indent + 2), str(name))
		ret += "] [TYPE:{0}]".format(self.irtype)
		return ret
