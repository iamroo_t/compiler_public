import llvmlite.ir as ir # type: ignore
import AST
import Type
from ErrWarnPrint import *

class TypeInferenceError(Exception): pass

class TypeInferer(object):
	def __init__(self):
		self.defaulttypemap = {}
		self.defaulttypemap["double"] = ir.DoubleType()
		self.defaulttypemap["float"] = ir.FloatType()
		self.defaulttypemap["none"] = ir.VoidType()
		self.defaulttypemap["bool"] = ir.IntType(1)
		self.defaulttypemap["char"] = ir.IntType(8)
		self.defaulttypemap["int8"] = ir.IntType(8)
		self.defaulttypemap["int16"] = ir.IntType(16)
		self.defaulttypemap["int32"] = ir.IntType(32)
		self.defaulttypemap["int64"] = ir.IntType(64)

		self.typemap = {}
		self.typemap["double"] = ir.DoubleType()
		self.typemap["float"] = ir.FloatType()
		self.typemap["none"] = ir.VoidType()
		self.typemap["any"] = ir.IntType(2)
		self.typemap["T"] = ir.IntType(3)
		self.typemap["bool"] = ir.IntType(1)
		self.typemap["char"] = ir.IntType(8)
		self.typemap["int8"] = ir.IntType(8)
		self.typemap["int16"] = ir.IntType(16)
		self.typemap["int32"] = ir.IntType(32)
		self.typemap["int64"] = ir.IntType(64)

		self.mapback = {}
		self.mapback[str(ir.DoubleType())] = "double"
		self.mapback[str(ir.FloatType())] = "float"
		self.mapback[str(ir.VoidType())] = "none"
		self.mapback[str(ir.IntType(2))] = "any"
		self.mapback[str(ir.IntType(3))] = "T"
		self.mapback[str(ir.IntType(1))] = "bool"
		self.mapback[str(ir.IntType(8))] = "char"
		self.mapback[str(ir.IntType(8))] = "int8"
		self.mapback[str(ir.IntType(16))] = "int16"
		self.mapback[str(ir.IntType(32))] = "int32"
		self.mapback[str(ir.IntType(64))] = "int64"

		self.immutablevarlist = []

		self.type_symtab = {}
		self.func_returntype_symtab = {}
		self.func_type_symtab = {}
		self.func_symtab = {}
		self.struct_field_tab = {}
		self.global_symtab = {}
		self.throwing_function_symtab = []
		self.current_function = ""

	'''def map_ir_to_type(node:ir.Type) -> Type.Type:
		print(dir(node))
		ret = Type.Type()
		ret.type = self.mapback[str(node)]
		return ret'''

	def _type_from_typemap(self, typestr: str) -> ir.Type:
		try:
			return self.typemap[typestr]
		except KeyError:
			raise TypeInferenceError("unknown type {0}".format(typestr))

	def map_type(self, node:Type.Type) -> ir.Type:
		assert(isinstance(node, Type.Type))
		t = self._type_from_typemap(node.type)
		for i in range(node.indirection):
			t = t.as_pointer()
		#if node.isarray and node.arraysize == 0:
		#	t = t.as_pointer()
		if node.isarray:
			if isinstance(node.arraysize, AST.ASTNode):
				self._infer(node.arraysize)
			t = ir.ArrayType(t, node.arraysize)
		return t


	def is_not_user_defined_type(self, type:str) -> bool:
		return type in self.defaulttypemap

	def _type_from_prototype(self, node:AST.PrototypeAST) -> ir.FunctionType:
		funcname = node.name
		# Create a function type
		args = []
		isvaargs = False
		for i in range(len(node.argtypes)):
			if node.argtypes[i].type == "vaargs" and i != len(node.argtypes) - 1:
				raise TypeInferenceError("vaargs must be at the end of function")

			if node.argtypes[i].type == "vaargs":
				isvaargs = True
				break

			if isinstance(node.argtypes[i].type, AST.PrototypeAST):
				t = self._type_from_prototype(node.argtypes[i].type)
				t = t.as_pointer()
			else:
				t = self.map_type(node.argtypes[i])
				if node.argtypes[i].type == "T":
					node.ispolymorphic = True
			self.func_symtab[node.argnames[i]] = t
			args.append(t)
		node.rettype = node.returntype
		node.returntype = self.map_type(node.returntype)
		self.func_returntype_symtab[funcname] = node.returntype
		if node.throws:
			self.throwing_function_symtab.append(node.name)
		return ir.FunctionType(node.returntype, args, isvaargs)

	#_infer modifies the nodes irtype attribute so we dont have to return anything
	def _infer(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		"""Node visitor. Dispathces upon node type.

		For AST node of class Foo, calls self._infer_Foo. Each visitor is
		expected to populate all nodes irtype attribute.
		"""
		method = '_infer_' + node.__class__.__name__
		#print("self.{0}({1}, {2})".format(method, node, parent))
		getattr(self, method)(node, parent)

	def _infer_ForExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.start_expr)
		self._infer(node.end_expr)
		self._infer(node.step_expr)
		self._infer(node.body)
		node.irtype = ir.VoidType()

	def _infer_WhileExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.end_expr)
		self._infer(node.body)
		node.irtype = ir.VoidType()

	def _infer_DoWhileExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.end_expr)
		self._infer(node.body)
		node.irtype = ir.VoidType()

	def _infer_IfExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.cond_expr)
		self._infer(node.then_expr)
		self._infer(node.else_expr)
		node.irtype = ir.VoidType()

	def _infer_BreakExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.VoidType()

	def _infer_ContinueExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.VoidType()

	def _infer_StringConstantAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.ArrayType(ir.IntType(8), len(node.val) + 1)

	def _infer_ArrayAccessExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.variable)
		self._infer(node.element)
		if isinstance(node.variable.irtype, ir.PointerType):
			node.irtype = node.variable.irtype.pointee
		elif isinstance(node.variable.irtype, ir.ArrayType):
			node.irtype = node.variable.irtype.element
		else:
			raise NotImplementedError

	def _infer_MemberExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.var)
		node.irtype = self.struct_field_tab[node.var.irtype.name][node.fieldname]

	def _infer_AddressofExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.expr)
		node.irtype = node.expr.irtype.as_pointer()

	def _infer_DereferenceExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.expr)
		node.irtype = node.expr.irtype.pointee

	def _infer_ImportAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		for ast in node.ast:
			self._infer(ast)

	def _infer_BinaryExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.lhs)
		self._infer(node.rhs)
		#if(self.mapback[str(node.lhs.irtype)] != self.mapback[str(node.rhs.irtype)]):
		#THIS WONT DO
		if(str(node.lhs.irtype) != str(node.rhs.irtype)):
			printErr("Mismatching types {3} {5} {4}\n\tat {0} {1}:{2}".format(node.lhs.sourceLocation.file, node.lhs.sourceLocation.line, node.lhs.sourceLocation.column, str(node.lhs.irtype), str(node.rhs.irtype), node.op))
		if isinstance(node.lhs, AST.VariableExprAST):
			if node.op in ['=', '+=', '+?=', '-=', '-?=', '*=', '*?=', '/=', '/?=', '%=', '%?=', '<<=', '>>=', '&=', '^=', '|=']:
				if node.lhs.name in self.immutablevarlist:
					printErr("mutating immutable variable {0}\n\tat {1} {2}:{3}".format(node.lhs.name, node.lhs.sourceLocation.file, node.lhs.sourceLocation.line, node.lhs.sourceLocation.column))
					#raise TypeInferenceError("mutating immutable variable {0}".format(node.lhs.name))
		node.irtype = node.lhs.irtype #check lhs rhs irtype is eq

	def _infer_DeferExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.body)
		node.irtype = node.body.irtype

	def _infer_StructAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		builder = ir.IRBuilder()
		context = ir.Context()
		module = ir.Module()
		struct = module.context.get_identified_type(node.name)
		elements = []
		for i in node.fieldtypes:
			if isinstance(i.type, AST.PrototypeAST):
				a = self._type_from_prototype(i.type)
				a = a.as_pointer()
			else:
				a = self.map_type(i)
			elements.append(a)
		telements = tuple(elements)
		struct.elements = telements
		self.typemap[node.name] = struct
		node.irtype = struct
		elementswithnames = {}
		for i, name in enumerate(node.fieldnames):
			elementswithnames[name] = telements[i]
		self.struct_field_tab[node.name] = elementswithnames

	def _infer_BooleanExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.IntType(1)

	def _infer_PrototypeAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = self._type_from_prototype(node)
		self.func_type_symtab[node.name] = node.irtype

	def _infer_GlobalVarExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		if isinstance(node.id_type.type, AST.PrototypeAST):
			self._infer(node.id_type.type)
			node.irtype = node.id_type.type.irtype.as_pointer()
		else:
			node.irtype = self.map_type(node.id_type)
			node.tctype = node.id_type
		self.func_symtab[node.name] = node.irtype
		if node.init:
			self._infer(node.init, node)
		self.global_symtab[node.name] = node.irtype

	def _infer_CastExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = self.map_type(node.type)
		node.tctype = node.type
		self._infer(node.expr)

	def _infer_FunctionAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		#if its an anonymous function save our function symtab for later continuation
		self.currentfunction = node.proto
		self.immutablevarlist = []
		if node.is_anonymous():
			savedfunc_symtab = self.func_symtab
		self.func_symtab = {}
		node.proto.irtype = self._type_from_prototype(node.proto)
		self._infer(node.body)
		self.func_returntype_symtab[node.proto.name] = node.proto.returntype
		self.func_type_symtab[node.proto.name] = node.proto.irtype
		#done? restore it
		if node.is_anonymous():
			self.func_symtab = savedfunc_symtab

	def _infer_ImplicitCastExpr(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self.func_symtab[node.argname] = self.map_type(node.id_type) #self.func_type_symtab[node.function].args[node.argcount]
		self._infer(node.body)
		del self.func_symtab[node.argname]
		node.irtype = self.map_type(node.id_type)
		node.tctype = node.id_type

	def _infer_NumberExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		if parent is not None:
			if not isinstance(parent.irtype, ir.VoidType):
				node.irtype = parent.irtype
			else:
				node.irtype = self.map_type(node.type)
		else:
			node.irtype = self.map_type(node.type)
		node.tctype = node.type

	def _infer_VariableExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		#FIXME
		try:
			node.irtype = self.func_symtab[node.name]
		except:
			try:
				node.irtype = self.func_type_symtab[node.name]
			except:
				try:
					node.irtype = self.global_symtab[node.name]
				except:
					raise TypeInferenceError("Cannot infere the type of a not yet defined variable: {0}".format(node.name))
	#checkTryCatch needed for calling directly from infer_TryExpr
	def _infer_CallExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None, checkTryCatch = True):
		try:
			node.irtype = self.func_type_symtab[node.callee].return_type
		except:
			try:
				node.irtype = self.func_symtab[node.callee].pointee.return_type
			except:
				try:
					node.irtype = self.global_symtab[node.callee].return_type
				except:
					if isinstance(node.callee, AST.MemberExprAST):
						self._infer(node.callee)
						node.irtype = node.callee.irtype
					#raise TypeInferenceError("Cannot infere the type to the call of a not defined function: {0}".format(node.callee))

		if node.catch:
			self.func_symtab[node.catch.val] = ir.IntType(64) #@FIXME change to ptr size later
			node.catch.irtype = ir.IntType(64) #@FIXME change to ptr size later
			self._infer(node.catch.body)

		if checkTryCatch and node.callee in self.throwing_function_symtab and node.catch is None:
			raise TypeInferenceError("throwing function: {0} not catch'ed or try'ed\n\tat {1} {2}:{3}".format(node.callee, node.sourceLocation.file, node.sourceLocation.line, node.sourceLocation.column))

		for arg in node.args:
			self._infer(arg, node)

	def _infer_UnaryExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.VoidType #FIXME cool for now

	def _infer_CompoundStatementAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		for ast in node.statements:
			self._infer(ast)
		node.irtype = ir.VoidType

	def _infer_VarExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		if isinstance(node.id_type.type, AST.PrototypeAST):
			self._infer(node.id_type.type)
			node.irtype = node.id_type.type.irtype.as_pointer()
		else:
			node.irtype = self.map_type(node.id_type)
			node.tctype = node.id_type
			if 'immutable' in node.id_type.qualifiers:
				self.immutablevarlist.append(node.name);
		self.func_symtab[node.name] = node.irtype
		if node.init:
			self._infer(node.init, node)

	def _infer_ReturnExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.body)
		node.irtype = node.body.irtype
		node.tctype = node.body.tctype

	def _infer_NoopAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = ir.IntType(32)

	def _infer_UndefinedValueAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		node.irtype = parent.irtype
		node.tctype = "undefined"

	def _infer_ASTNode(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		pass

	def _infer_ThrowExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		if not self.currentfunction.throws:
			printErr("Throw in function:{0} that does not throw\n\tat {1} {2}:{3}".format(self.currentfunction.name, node.sourceLocation.file, node.sourceLocation.line, node.sourceLocation.column))

	def _infer_TryExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer_CallExprAST(node.body, None, False)
		node.irtype = node.body.irtype

	def _infer_ComptimeExprAST(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node.body)
		node.irtype = node.body.irtype

	def inferType(self, node:AST.ASTNode, parent:AST.ASTNode = None):
		self._infer(node)
