import Lexer
import Type
import AST

import os
import typing
from ErrWarnPrint import printWarn, printErr

class ParseError(Exception): pass


class Parser(object):
	"""After the parser is created, invoke parse_toplevel multiple times to parse
	source into an AST.
	"""
	def __init__(self, import_dirs:typing.List[str]):
		self.lexer:Lexer.Lexer = None # type: ignore
		self.token_generator:typing.Generator[Lexer.Token, None, None] = None # type: ignore
		self.cur_tok:Lexer.Token = None # type: ignore
		self.import_dirs:typing.List[str] = import_dirs
		self.imported_files = []
		self.import_recursion_counter = 0
		self.recursed_file_name = [] #list of files to print recursion error trace

	def give_buf(self, buf:str, path:str):
		self.lexer = Lexer.Lexer(buf, path)
		self.token_generator = self.lexer.tokens()
		self.cur_tok = None #Lexer.Token(Lexer.TokenKind.EOF, "")
		self._get_next_token()

	# toplevel ::= definition | external | expression | ';'
	def parse_toplevel(self, buf = ""):
		"""Given a string, returns an AST node representing it."""
		if self.cur_tok.kind == Lexer.TokenKind.EXTERN:
			r = self._parse_external()
			self._match(Lexer.TokenKind.SEMICOLON)
			return r
		elif self.cur_tok.kind == Lexer.TokenKind.DEF:
			return self._parse_definition()
		elif self.cur_tok.kind == Lexer.TokenKind.EOF:
			return None
		elif self.cur_tok.kind == Lexer.TokenKind.INTERNAL:
			self._get_next_token() #consume the internal
			if self.cur_tok.value == "function":
				return self._parse_definition("internal")
			elif self.cur_tok.value == "global":
				self._get_next_token() #consume the global
				return self._parse_global_variable("internal")
		elif self.cur_tok.kind == Lexer.TokenKind.GLOBAL:
			self._get_next_token() #consume the global
			return self._parse_global_variable()
		else:
			return self._parse_toplevel_expression()

	def _get_next_token(self) -> None:
		self.cur_tok = next(self.token_generator) # type: ignore

	def _match(self, expected_kind:Lexer.TokenKind, expected_value:typing.Optional[str]=None) -> None:
		"""Consume the current token; verify that it's of the expected kind.

		If expected_kind == Lexer.TokenKind.OPERATOR, verify the operator's value.
		"""
		if (expected_kind == Lexer.TokenKind.OPERATOR and self.cur_tok.kind != Lexer.TokenKind.OPERATOR):
			raise ParseError(self.lexer.get_current() + 'Expected "{0}" got "{1}"'.format(expected_value, self.lexer.map_token(self.cur_tok.kind)))

		elif (expected_kind == Lexer.TokenKind.OPERATOR and not self._cur_tok_is_operator(expected_value)):
			raise ParseError(self.lexer.get_current() + 'Expected "{0}" got "{1}"'.format(expected_value, self.cur_tok.value))
		elif expected_kind != self.cur_tok.kind:
			first = self.lexer.map_token(expected_kind)
			second = self.lexer.map_token(self.cur_tok.kind)
			if self.cur_tok.kind == Lexer.TokenKind.OPERATOR:
				second = "operator " + self.cur_tok.value
			raise ParseError(self.lexer.get_current() + 'Expected "{0}" before "{1}"'.format(first, second))
		self._get_next_token()

	_precedence_map = {'=': 2, '+=':2, '+?=':2, '-=':2, '-?=':2, '*=':2, '*?=':2, '/=':2, '/?=':2, '%=':2, '%?=':2, '<<=':2, '>>=':2, '&=':2, '^=':2, '|=':2, '||':4, '&&':5, '|':6, '^':7, '&':8, '==': 9, '!=': 9, '<': 10, '>': 10, '<<':15, '>>':15, '+': 20, '+?':20, '-': 20, '-?':20, '*': 40, '*?':40, '/':40, '/?':40, '%':40, '%?':40}

	def _cur_tok_precedence(self) -> int:
		"""Get the operator precedence of the current token."""
		try:
			return self._precedence_map[self.cur_tok.value]
		except KeyError:
			return -1

	def _cur_tok_is_operator(self, op:typing.Optional[str]) -> bool:
		"""Query whether the current token is the operator op"""
		return (self.cur_tok.kind == Lexer.TokenKind.OPERATOR and
				self.cur_tok.value == op)

	def _parse_type(self) -> Type.Type:
		qualifiers = []
		isarray = False
		arraysize = 0
		while self.cur_tok.kind == Lexer.TokenKind.QUALIFIER:
			qualifiers.append(self.cur_tok.value)
			self._get_next_token()
		id_type = self.cur_tok.value
		if id_type == "function":
			self._get_next_token() #consume the 'function'
			id_type = self._parse_prototype()
		else:
			self._get_next_token()
		indirection = 0
		while self.cur_tok.kind == Lexer.TokenKind.PTR:
			indirection += 1
			self._get_next_token()
		if self.cur_tok.kind == Lexer.TokenKind.BRACKETOPEN:
			isarray = True
			self._get_next_token()
			if self.cur_tok.kind != Lexer.TokenKind.BRACKETCLOSE:
				try:
					arraysize = int(self.cur_tok.value)
				except ValueError:
					printErr("Size of array must be an integer, array will have size of 1")
					arraysize = 1
				self._get_next_token()

			else:
				arraysize = 0
			self._match(Lexer.TokenKind.BRACKETCLOSE)
		r = Type.Type(id_type, indirection, qualifiers, isarray, arraysize)
		return r

	def _parse_inline_call_expr(self) -> AST.CallExprAST:
		self._get_next_token() #consume the 'inline'
		id_name = self.cur_tok.value

		self._get_next_token()
		self._match(Lexer.TokenKind.PARENOPEN)
		args = []
		while not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
			args.append(self._parse_expression(False))
			if not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
				self._match(Lexer.TokenKind.OPERATOR, ',')

		#self._get_next_token()  # consume the ')'
		self._match(Lexer.TokenKind.PARENCLOSE)
		ret = AST.CallExprAST(id_name, args, None, True)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_global_variable(self, linkage="external") -> AST.GlobalVarExprAST:
		id_name = self.cur_tok.value
		self._get_next_token()
		self._get_next_token() #consume the ':'
		id_type = self._parse_type()
		id_init = None

		if self._cur_tok_is_operator('='):
			self._get_next_token()
			id_init = self._parse_expression(False)
		ret = AST.GlobalVarExprAST(id_name, id_type, id_init, linkage)
		ret.sourceLocation = self.lexer.location()
		self._match(Lexer.TokenKind.SEMICOLON)
		return ret

	def _parse_member_expr(self, id_name) -> AST.MemberExprAST:
		self._get_next_token() # consume the '.'
		id_field = self.cur_tok.value
		self._get_next_token()
		if self.cur_tok.kind == Lexer.TokenKind.PARENOPEN:
			self._get_next_token() #consume the '('
			args = []
			while not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
				args.append(self._parse_expression(False))
				if not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
					self._match(Lexer.TokenKind.OPERATOR, ',')

			#self._get_next_token()  # consume the ')'
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.CallExprAST(AST.MemberExprAST(id_name, id_field), args)
			ret.sourceLocation = self.lexer.location()
			return ret
		ret = AST.MemberExprAST(id_name, id_field)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_array_access_expr(self, id_name) -> AST.ArrayAccessExprAST:
		self._get_next_token() #consume the bracket
		accessedelement = self._parse_expression(False)
		self._match(Lexer.TokenKind.BRACKETCLOSE)
		return AST.ArrayAccessExprAST(id_name, accessedelement)

	# identifierexpr
	#   ::= identifier
	#   ::= identifier '(' expression* ')'
	def _parse_identifier_expr(self) -> AST.ASTNode:
		id_name = self.cur_tok.value
		self._get_next_token()
		#FIXME
		# If followed by a '(' it's a cast, dereference or a call; if by a ':' its a variable declaration or struct definition otherwise, a simple variable ref.

		ret = None
		if self.cur_tok.kind == Lexer.TokenKind.BRACKETOPEN:
			ret = self._parse_array_access_expr(AST.VariableExprAST(id_name))

		elif self.cur_tok.value == '.':
			ret = self._parse_member_expr(AST.VariableExprAST(id_name))

		while self.cur_tok.kind == Lexer.TokenKind.BRACKETOPEN or self.cur_tok.value == '.':
			if self.cur_tok.value == '.':
				ret = self._parse_member_expr(ret)
			else:
				ret = self._parse_array_access_expr(ret)

		if ret:
			return ret

		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN and id_name == "__builtin__intrinsic":
			self._get_next_token()
			intrinsic = self.cur_tok.value
			self._get_next_token()
			exprs = []
			while not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
				self._match(Lexer.TokenKind.OPERATOR, ',')
				exprs.append(self._parse_expression(False))
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.IntrinsicExprAST(intrinsic, exprs)
			ret.sourceLocation = self.lexer.location()
			return ret

		#its a cast
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN and id_name == "cast":
			self._get_next_token()
			type = self._parse_type()
			self._match(Lexer.TokenKind.OPERATOR, ',')
			expr = self._parse_expression(False)
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.CastExprAST(type, expr)
			ret.sourceLocation = self.lexer.location()
			return ret

		#its a dereference
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN and id_name == "dereference":
			self._get_next_token() #consume the leading (
			expr = self._parse_expression(False)
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.DereferenceExprAST(expr)
			ret.sourceLocation = self.lexer.location()
			return ret

		#its an addressof
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN and id_name == "addressof":
			self._get_next_token() #consume the leading '('
			expr = self._parse_expression(False)
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.AddressofExprAST(expr)
			ret.sourceLocation = self.lexer.location()
			return ret

		#its a typeof
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN and id_name == "typeof":
			self._get_next_token() #consume the leading '('
			expr = self._parse_expression(False)
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.TypeofExprAST(expr)
			ret.sourceLocation = self.lexer.location()
			return ret

		#its a call
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN:
			self._get_next_token() #consume the '('
			args = []
			while not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
				args.append(self._parse_expression(False))
				if not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
					self._match(Lexer.TokenKind.OPERATOR, ',')

			#self._get_next_token()  # consume the ')'
			self._match(Lexer.TokenKind.PARENCLOSE)
			ret = AST.CallExprAST(id_name, args, None)
			ret.sourceLocation = self.lexer.location()
			if(self.cur_tok.kind == Lexer.TokenKind.OPERATOR and self.cur_tok.value == '=>'):
				self._get_next_token();
				catch = self._parse_catch_expression()
				ret.catch = catch
			return ret

		#its a variable or struct decleration
		elif self.cur_tok.kind == Lexer.TokenKind.DOUBLECOLON:
			self._get_next_token() #consume the ':'
			id_type = self._parse_type()
			#its a struct declaration
			if id_type.type == 'struct':
				fieldnames = []
				fieldtypes = []
				self._match(Lexer.TokenKind.CURLYOPEN)
				while not self.cur_tok.kind == Lexer.TokenKind.CURLYCLOSE:
					fieldnames.append(self.cur_tok.value)
					self._get_next_token()
					self._match(Lexer.TokenKind.DOUBLECOLON)
					#self._get_next_token()
					fieldtypes.append(self._parse_type())
					self._match(Lexer.TokenKind.SEMICOLON)
				self._match(Lexer.TokenKind.CURLYCLOSE)
				if not fieldnames or not fieldtypes:
					raise ParseError(self.lexer.get_current() + "Cannot declare an empty struct")
				ret = AST.StructAST(id_name, fieldnames, fieldtypes)
				ret.sourceLocation = self.lexer.location()
				return ret
			if id_type.type == 'enum':
				fieldnames = []
				self._match(Lexer.TokenKind.CURLYOPEN)
				while not self.cur_tok.kind == Lexer.TokenKind.CURLYCLOSE:
					fieldnames.append(self.cur_tok.value)
					self._get_next_token()
					self._match(Lexer.TokenKind.DOUBLECOLON)
					#self._get_next_token()
					fieldtypes.append(self._parse_type())
					self._match(Lexer.TokenKind.SEMICOLON)
				self._match(Lexer.TokenKind.CURLYCLOSE)
				if not fieldnames or not fieldtypes:
					raise ParseError(self.lexer.get_current() + "Cannot declare an empty enum")
				ret = AST.StructAST(id_name, fieldnames, fieldtypes)
				ret.sourceLocation = self.lexer.location()
				return ret
			#its a variable declaration
			else:
				id_init = None

				if self._cur_tok_is_operator('='):
					self._get_next_token()
					id_init = self._parse_expression(False)
				ret = AST.VarExprAST(id_name, id_type, id_init)
				ret.sourceLocation = self.lexer.location()
				return ret

		#its a variable reference
		else:
			ret = AST.VariableExprAST(id_name)
			ret.sourceLocation = self.lexer.location()
			return ret

	# numberexpr ::= number
	def _parse_number_expr(self) -> AST.NumberExprAST:
		type:Type.Type
		if '.' in self.cur_tok.value:
			type = Type.Type("double", 0, [], False, 0)
			if 'f' in self.cur_tok.value:
				type = Type.Type("float", 0, [], False, 0)
		else:
			type = Type.Type("int32", 0, [], False, 0)
		result = AST.NumberExprAST(self.cur_tok.value, type)
		result.sourceLocation = self.lexer.location()
		self._get_next_token()  # consume the number
		return result

	# parenexpr ::= '(' expression ')'
	def _parse_paren_expr(self) -> AST.ASTNode:
		self._get_next_token()  # consume the '('
		expr = self._parse_expression(False)
		expr.sourceLocation = self.lexer.location()
		self._match(Lexer.TokenKind.PARENCLOSE)
		return expr

	# primary
	#   ::= identifierexpr
	#   ::= numberexpr
	#   ::= parenexpr
	#   ::= ifexpr
	#   ::= forexpr
	def _parse_primary(self) -> AST.ASTNode:
		if self.cur_tok.kind == Lexer.TokenKind.IDENTIFIER:
			return self._parse_identifier_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.NUMBER:
			return self._parse_number_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.PARENOPEN:
			return self._parse_paren_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.IF:
			return self._parse_if_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.ELSE:
			raise ParseError(self.lexer.get_current() + 'else without a previous if')
		elif self.cur_tok.kind == Lexer.TokenKind.FOR:
			return self._parse_for_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.WHILE:
			return self._parse_while_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.DO:
			return self._parse_do_while_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.RETURN:
			return self._parse_return_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.NAMESPACE:
			return self._parse_namespace_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.IMPORT:
			return self._parse_import_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.DEFER:
			return self._parse_defer_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.CURLYOPEN:
			return self._parse_compound_statement()
		elif self.cur_tok.kind == Lexer.TokenKind.STRING:
			return self._parse_string_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.INLINE:
			return self._parse_inline_call_expr()
		elif self.cur_tok.kind == Lexer.TokenKind.TRUE:
			self._get_next_token() #consume the 'true'
			return AST.BooleanExprAST(True)
		elif self.cur_tok.kind == Lexer.TokenKind.FALSE:
			self._get_next_token() #consume the 'false'
			return AST.BooleanExprAST(False)
		elif self.cur_tok.kind == Lexer.TokenKind.SWITCH:
			return self._parse_switch_statement()
		elif self.cur_tok.kind == Lexer.TokenKind.DEF:
			self._get_next_token() # consume the 'function'
			proto = self._parse_prototype()
			proto.irtype = ""
			expr = self._parse_expression() #self._parse_compound_statement()
			ret = AST.FunctionAST.create_anonymous(expr, proto)
			ret.sourceLocation = self.lexer.location()
			return ret
		elif self.cur_tok.kind == Lexer.TokenKind.EOF:
			raise ParseError(self.lexer.get_current() + 'Unexpectedly reached end of file\nThere must be something wrong with the Lexer or with the way the compiler feeds the code to the Lexer')
		elif self.cur_tok.kind == Lexer.TokenKind.BREAK:
			self._get_next_token() #consume the 'break'
			return AST.BreakExprAST()
		elif self.cur_tok.kind == Lexer.TokenKind.CONTINUE:
			self._get_next_token() #consume the 'continue'
			return AST.ContinueExprAST()
		elif self.cur_tok.kind == Lexer.TokenKind.IMPLICITCAST:
			return self._parse_implicit_cast_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.UNDEFINED:
			self._get_next_token()
			return AST.UndefinedValueAST()
		elif self.cur_tok.kind == Lexer.TokenKind.THROW:
			return self._parse_throw_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.TRY:
			return self._parse_try_expression()
		elif self.cur_tok.kind == Lexer.TokenKind.COMPTIME:
			return self._parse_comptime_expression()
		else:
			raise ParseError(self.lexer.get_current() + 'Unknown {0} when expecting an expression\n'.format(self.lexer.map_token(self.cur_tok.kind)))

	def _find_import_file(self, name):
		ret = []
		for i in self.import_dirs:
			for root, dirs, files in os.walk(i):
				if name.replace('.als', '') in dirs:
					for root2, dirs2, files2 in os.walk(name.replace('.als', '')):
						for file in files2:
							ret.append(os.path.join(root, root2, file))
				if name in files:
					ret.append(os.path.join(root, name))
		return ret

	def _parse_comptime_expression(self) -> AST.ComptimeExprAST:
		self._get_next_token() #consume the 'comptime'
		body = self._parse_expression(False)
		return AST.ComptimeExprAST(body)

	def _parse_string_expression(self) -> AST.StringConstantAST:
		strr = self.cur_tok.value
		self._get_next_token()
		ret = AST.StringConstantAST(strr);
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_import_expression(self) -> AST.ImportAST:
		if self.import_recursion_counter > 10:
			printErr("Recursive import in {0}".format(self.recursed_file_name));
			exit()
		self.import_recursion_counter += 1

		self._get_next_token() #consume the 'import'
		name = []
		name.append(self.cur_tok.value)
		self._get_next_token()

		while self.cur_tok.kind == Lexer.TokenKind.NUMBER:
			self._get_next_token()
			name.append(self.cur_tok.value)
			self._get_next_token()

		self._match(Lexer.TokenKind.SEMICOLON)

		p = Parser(self.import_dirs)
		p.import_recursion_counter = self.import_recursion_counter
		codestr = None

		importfiles = self._find_import_file(str(name[-1] + ".als"))
		for i in importfiles:
			if i in self.imported_files:
				#printWarn("{0} imported multiple times".format(i)) #Don't warn if we don't know if it came from the user or a recursive import
				importfiles.remove(i)
				continue
			self.imported_files.append(i)

		p.imported_files += self.imported_files #this line fixes recursive imports but throws no errors what so ever, I'll keep the recursion handler tho, maybe when I have the time I can warn the user about a half assed import

		try:
			l = []

			for file in importfiles:
				with open(file, "r") as inputFile:
					codestr = inputFile.read()
					p.give_buf(codestr, str(file))

					ast = p.parse_toplevel()
					while ast != None:
						l.append(ast)
						ast = p.parse_toplevel()

			ret = AST.ImportAST(name, l)

			self.import_recursion_counter = 0
			return ret

		except:
			raise ParseError('Module {0} not found'.format('/'.join(name)))
		self.import_recursion_counter = 0
		return AST.ASTNode()

	def _parse_implicit_cast_expression(self):
		self._get_next_token() #consume the 'implicitcast'
		self._match(Lexer.TokenKind.PARENOPEN)
		function = self.cur_tok.value
		self._get_next_token()
		self._match(Lexer.TokenKind.OPERATOR, ',')
		argcount = int(self.cur_tok.value)
		self._get_next_token()
		self._match(Lexer.TokenKind.OPERATOR, ',')
		argtype = self._parse_type()
		self._match(Lexer.TokenKind.OPERATOR, ',')
		argname = self.cur_tok.value
		self._get_next_token()
		self._match(Lexer.TokenKind.PARENCLOSE)
		howto = self._parse_compound_statement()
		return AST.ImplicitCastExpr(function, argcount, argtype, argname, howto)

	#FIXME implement this
	def _parse_switch_statement(self):# -> AST.SwitchStatementAST:
		return

	def _parse_namespace_expression(self) -> AST.NamespaceAST:
		self._get_next_token() #consume the 'namespace'
		name = self.cur_tok.value;
		self._get_next_token()
		self._match(Lexer.TokenKind.SEMICOLON)
		return AST.NamespaceAST(name)

	def _parse_defer_expression(self) -> AST.DeferExprAST:
		self._get_next_token() #consume the 'defer'
		expr = self._parse_expression(False)
		ret = AST.DeferExprAST(expr)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_return_expression(self) -> AST.ReturnExprAST:
		self._get_next_token() #consume the 'return'
		if self.cur_tok.kind != Lexer.TokenKind.SEMICOLON:
			expr = self._parse_expression(False)
		else:
			expr = AST.NoopAST()
		ret = AST.ReturnExprAST(expr)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_throw_expression(self) -> AST.ThrowExprAST:
		self._get_next_token() #consume the 'throw'
		if self.cur_tok.kind != Lexer.TokenKind.SEMICOLON:
			expr = self._parse_expression(False)
		else:
			raise ParseError('empty throw, throw needs a value')
		ret = AST.ThrowExprAST(expr)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_try_expression(self) -> AST.TryExprAST:
		self._get_next_token() #consume the 'try'
		if self.cur_tok.kind != Lexer.TokenKind.SEMICOLON:
			expr = self._parse_expression(False)
		else:
			raise ParseError('empty try, try needs a function call after it')
		ret = AST.TryExprAST(expr)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_catch_expression(self) -> AST.CatchExprAST:
		#self._get_next_token() #consume the 'catch'
		self._match(Lexer.TokenKind.CATCH)

		self._match(Lexer.TokenKind.PARENOPEN)
		newvar = self.cur_tok.value
		self._get_next_token()
		self._match(Lexer.TokenKind.PARENCLOSE)
		expr = self._parse_compound_statement()

		ret = AST.CatchExprAST(newvar, expr)
		ret.sourceLocation = self.lexer.location()

		return ret

	def _parse_compound_statement(self) -> AST.CompoundStatementAST:
		location = self.lexer.location()
		self._match(Lexer.TokenKind.CURLYOPEN)

		expressions = []
		while self.cur_tok.kind != Lexer.TokenKind.CURLYCLOSE:
			ap = self._parse_expression()
			expressions.append(ap)
			#if not isinstance(ap, (AST.IfExprAST, AST.ForExprAST, AST.CompoundStatementAST, AST.WhileExprAST, AST.DoWhileExprAST)):
			#	self._match(Lexer.TokenKind.SEMICOLON)

		#if expressions == []:
		#	raise ParseError("empty {..} block")

		self._match(Lexer.TokenKind.CURLYCLOSE)
		ret = AST.CompoundStatementAST(expressions)
		ret.sourceLocation = location
		#print("Parser Lexical {0} {1}".format(location.line, location.column))
		return ret

	# ifexpr ::= 'if' expression { expression }
	def _parse_if_expr(self) -> AST.IfExprAST:
		ret = ""
		self._get_next_token()  # consume the 'if'
		self._match(Lexer.TokenKind.PARENOPEN)
		cond_expr:AST.ASTNode = self._parse_expression(False)
		cond_expr.sourceLocation = self.lexer.location()
		self._match(Lexer.TokenKind.PARENCLOSE)
		then_expr:AST.ASTNode = self._parse_expression(False)
		then_expr.sourceLocation = self.lexer.location()
		#then_expr = self._parse_expression()

		else_expr:AST.ASTNode = AST.NoopAST()
		has_else:bool = False
		else_expr.sourceLocation = self.lexer.location()
		if self.cur_tok.kind == Lexer.TokenKind.ELSE:
			has_else = True
			self._get_next_token()  # consume the 'else'

			else_expr = self._parse_expression()
			else_expr.sourceLocation = self.lexer.location()

		ret = AST.IfExprAST(cond_expr, then_expr, has_else, else_expr)
		ret.sourceLocation = self.lexer.location()
		return ret

	# forexpr ::= 'for' identifier '=' expr ',' expr (',' expr)? 'in' expr
	def _parse_for_expr(self) -> AST.ForExprAST:
		self._get_next_token()  # consume the 'for'
		#id_name = self.cur_tok.value
		start_expr:AST.ASTNode = AST.NoopAST()
		end_expr:AST.ASTNode = AST.NoopAST()
		step_expr:AST.ASTNode = AST.NoopAST()
		self._match(Lexer.TokenKind.PARENOPEN)
		if self.cur_tok.kind != Lexer.TokenKind.SEMICOLON:
			start_expr = self._parse_expression()
		if self.cur_tok.kind != Lexer.TokenKind.SEMICOLON:
			end_expr = self._parse_expression()
		if not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
			step_expr = self._parse_expression(False)
		self._match(Lexer.TokenKind.PARENCLOSE)
		body = self._parse_expression()
		ret =  AST.ForExprAST(start_expr, end_expr, step_expr, body)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_while_expr(self) -> AST.WhileExprAST:
		self._get_next_token()  # consume the 'while'
		self._match(Lexer.TokenKind.PARENOPEN)
		end_expr = self._parse_expression(False)
		self._match(Lexer.TokenKind.PARENCLOSE)
		body = self._parse_expression()
		ret =  AST.WhileExprAST(end_expr, body)
		ret.sourceLocation = self.lexer.location()
		return ret

	def _parse_do_while_expr(self) -> AST.DoWhileExprAST:
		self._get_next_token()  # consume the 'do'
		body = self._parse_expression()
		self._match(Lexer.TokenKind.WHILE)
		self._match(Lexer.TokenKind.PARENOPEN)
		end_expr = self._parse_expression(False)
		self._match(Lexer.TokenKind.PARENCLOSE)
		ret =  AST.DoWhileExprAST(end_expr, body)
		ret.sourceLocation = self.lexer.location()
		return ret

	# unary
	#   ::= primary
	#   ::= <op> unary
	def _parse_unary(self) -> AST.ASTNode:
		# no unary operator before a primary
		if not self.cur_tok.kind == Lexer.TokenKind.OPERATOR:
			return self._parse_primary()

		# unary operator
		op = self.cur_tok.value
		self._get_next_token()
		ret = AST.UnaryExprAST(op, self._parse_unary())
		ret.sourceLocation = self.lexer.location()
		return ret

	# binoprhs ::= (<binop> primary)*
	def _parse_binop_rhs(self, expr_prec, lhs) -> AST.ASTNode:
		"""Parse the right-hand-side of a binary expression.

		expr_prec: minimal precedence to keep going (precedence climbing).
		lhs: AST of the left-hand-side.
		"""
		while True:
			cur_prec = self._cur_tok_precedence()
			# If this is a binary operator with precedence lower than the
			# currently parsed sub-expression, bail out. If it binds at least
			# as tightly, keep going.
			# Note that the precedence of non-operators is defined to be -1,
			# so this condition handles cases when the expression ended.
			if cur_prec < expr_prec:
				return lhs
			op = self.cur_tok.value
			self._get_next_token()  # consume the operator
			rhs = self._parse_unary()

			next_prec = self._cur_tok_precedence()
			# There are three options:
			# 1. next_prec > cur_prec: we need to make a recursive call
			# 2. next_prec == cur_prec: no need for a recursive call, the next
			#	iteration of this loop will handle it.
			# 3. next_prec < cur_prec: no need for a recursive call, combine
			#	lhs and the next iteration will immediately bail out.
			if cur_prec < next_prec:
				rhs = self._parse_binop_rhs(cur_prec + 1, rhs)

			# Merge lhs/rhs
			lhs = AST.BinaryExprAST(op, lhs, rhs)

	# expression ::= primary binoprhs
	def _parse_expression(self, withsemicolon = True) -> AST.ASTNode:
		lhs = self._parse_unary()
		# Start with precedence 0 because we want to bind any operator to the
		# expression at this point.
		ret = self._parse_binop_rhs(0, lhs)
		ret.sourceLocation = self.lexer.location()
		if not isinstance(ret, (AST.CompoundStatementAST, AST.ImportAST, AST.ImplicitCastExpr, AST.IfExprAST, AST.ForExprAST, AST.WhileExprAST, AST.DoWhileExprAST)) and withsemicolon:
			self._match(Lexer.TokenKind.SEMICOLON)
		return ret


	# prototype
	#   ::= id '(' id* ')'
	#   ::= 'binary' LETTER number? '(' id id ')'
	def _parse_prototype(self) -> AST.PrototypeAST:
		prec = 30
		if self.cur_tok.kind == Lexer.TokenKind.IDENTIFIER:
			name = self.cur_tok.value
			self._get_next_token()
		elif self.cur_tok.kind == Lexer.TokenKind.UNARY:
			self._get_next_token()
			if self.cur_tok.kind != Lexer.TokenKind.OPERATOR:
				raise ParseError(self.lexer.get_current() + 'Expected operator after "unary"')
			name = 'unary{0}'.format(self.cur_tok.value)
			self._get_next_token()
		elif self.cur_tok.kind == Lexer.TokenKind.BINARY:
			self._get_next_token()
			if self.cur_tok.kind != Lexer.TokenKind.OPERATOR:
				raise ParseError(self.lexer.get_current() + 'Expected operator after "binary"')
			name = 'binary{0}'.format(self.cur_tok.value)
			self._get_next_token()

			# Try to parse precedence
			if self.cur_tok.kind == Lexer.TokenKind.NUMBER:
				prec = int(self.cur_tok.value)
				if not (0 < prec < 101):
					raise ParseError(self.lexer.get_current() + 'Invalid precedence', prec)
				self._get_next_token()
			# Add the new operator to our precedence table so we can properly
			# parse it.
			realname = name.replace("unary", "").replace("binary", "").replace(" ", "")
			self._precedence_map[realname] = prec
		else:
			name = "got_anonymous_function"
			#return AST.FunctionAST.create_anonymous(expr, proto)


		self._match(Lexer.TokenKind.PARENOPEN)
		argnames = []
		argtypes = []
		while not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
			indirection = 0
			argnames.append(self.cur_tok.value)
			self._get_next_token() # consume ":"
			self._get_next_token()
			a = self._parse_type()
			argtypes.append(a)
			if not self.cur_tok.kind == Lexer.TokenKind.PARENCLOSE:
				self._match(Lexer.TokenKind.OPERATOR, ',')
			else:
				break

		self._match(Lexer.TokenKind.PARENCLOSE)
		self._match(Lexer.TokenKind.RETURNTYPE, '->')
		rettype = self._parse_type()
		doesthrow = False
		if self.cur_tok.kind == Lexer.TokenKind.THROWS:
			doesthrow = True
			self._get_next_token()

		if name.startswith('binary') and len(argnames) != 2:
			raise ParseError(self.lexer.get_current() + 'Expected binary operator to have 2 operands')
		elif name.startswith('unary') and len(argnames) != 1:
			raise ParseError(self.lexer.get_current() + 'Expected unary operator to have one operand')

		ret = AST.PrototypeAST(
name, argnames, argtypes, rettype, doesthrow, name.startswith(('unary', 'binary')), prec)
		ret.sourceLocation = self.lexer.location()
		return ret

	# external ::= 'extern' prototype
	def _parse_external(self) -> AST.PrototypeAST:
		self._get_next_token()  # consume 'extern'
		self._get_next_token()  # consume 'function'
		return self._parse_prototype()

	# definition ::= 'function' prototype expression
	def _parse_definition(self, internal = "external") -> AST.FunctionAST:
		self._get_next_token()  # consume 'function'
		proto = self._parse_prototype()
		exprs = self._parse_expression()
		ret = AST.FunctionAST(proto, exprs, proto.returntype, internal)
		ret.sourceLocation = proto.sourceLocation
		return ret

	# toplevel ::= expression
	def _parse_toplevel_expression(self) -> AST.ASTNode:
		return self._parse_expression()
