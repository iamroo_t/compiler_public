//maybe if '-> type' is omitted from function make it void?
//also i32 f32 etc instead of int32?

/*comptime {
	add_library("SDL2");
	emit_debug_info(true);
	optimization_level(0);
	output_filename("test");
}*/

import std;

import std.io;

//import std;

//import tests;

//file local global
internal global intglob: int32 = 5;

external function printf(fmt:immutable char ptr, va:vaargs) -> int32; //multiple external declerations are OK

implicitcast(printf, 0, char[13], arg) {
	//arg gets passed by value so cast(char ptr, arg) wont work
	cast(char ptr, addressof(arg[0]));
}

implicitcast(printf, 0, char[4], arg) {
	cast(char ptr, addressof(arg[0]));
}

//will match arrays of any size
implicitcast(printf, 0, char[], arg) {
	cast(char ptr, addressof(arg[0]));
}

//                 precedence
function binary <=> 60 (x:int32, y:int32) -> int32 {
	return x + y;
}

//polymorphic function only generated on calls
function tswap(x:T ptr, y:T ptr) -> none {
	tmp:T = dereference(x);
	dereference(x) = dereference(y);
	dereference(y) = tmp;
}

//file local function
internal function swap(x:int32 ptr, y:int32 ptr) -> none {
	tmp:int32 = dereference(x);
	dereference(x) = dereference(y);
	dereference(y) = tmp;
}

function asd(b:bool) -> bool {
	a:bool = true;
	return a;
}

function firstclassfunctionstest(addmyname:function (a:int32, b:int32) -> int32, x:int32, y:int32) -> int32 {
	return addmyname(x, y);
}

function add(x:int32, y:int32) -> int32 {
	return x + y;
}

vec2:struct {
	x:float;
	y:float;
};

ivec2:struct {
	x:int32;// @doubleme;
	y:int32;
};

ivec2witharr:struct {
	x:int32;
	y:int32;
	z:ivec2[2];
};

/*testenum:enum {
	TEST,
	ENUM
};

function testenumf(a:testenum) {
	if(a == TEST) {
		printf("Got TEST\n");
	}
	else {
		printf("Got ENUM\n");
	}
}*/

//should match arrays of any size, currently WIP
function testarrayanysize(x:char[]) -> none {
	return;
}

function testarrayanysizenoimplicit(x:char[5]) -> none {
	return;
}

function testasm(x:int32, y:int32) -> int32 {
	/*__builtin__intrinsic(asm, "
		mov 2, 0
		add 1, 0");*/
}

function typetest(t:any) -> none {
	other:any = t;
	//asd:char ptr = typeof(t));
	//printf("type of t:%s", typeof(t));
	//printf("type of t:%s", typeof(other)));
}

function printodd(n:int32) -> none {
	printf("odd numbers from 0 to %d are :", n);
	for(i:int32 = 0; i < n; i +?= 1) {
		if(i % 2 == 0) {
			//printf("%d ", i);
			continue;
			printf("i am dead code\n");
		}
		printf("%d ", i);
	}
	printf("\n");
}

/*function alloc() -> none {
	a:int64 = 5
	ignore a:int64 = 5 //ignore warnings(here shadowing)
	t:int32 ptr = malloc(4);
	delete t; //ondelete() for types?
}*/

function retundef() -> int32 {
	undefval:int32 = undefined;
	printf("value of undefined int32 %d\n", undefval);
	return undefval;
}

function maxerr(a:int64, b:int64) -> int64 throws {
	if(a > b) {
		return a;
	}
	else {
		throw bIsBigger;
	}
}

function lambdalike(a:int32) -> int32 return a;

//errors
/*function max(a:int32, b:int32) -> int32 {
	if(a > b) {
		return a;
	}
	else {
		throw bIsBigger;
	}
}*/

/*function fibrec(n:int32) -> int32 {
	if(n == 0){
		return 0;
	}
	else if(n == 1) {
		return 1;
	}
	else {
		return (fibrec(n - 1) + fibrec(n - 2));
	}
}*/

function fib(n:int32) -> int32 {
	ret:int32;
	t1:int32 = 0;
	t2:int32 = 1;
	tmp:int32;

	//defer return ret;

	//printf("0 1 ");

	for(i:int32 = 0; i < n + 1; i += 1) {
		tmp = t1;
		t1 = t2;
		t2 = t1 + tmp;
		//printf("%d ", t2);
		ret = t2;
	}
	//printf("\n");
	return ret;
}

function main(argc:int32, argv:char ptr ptr) -> int32 {
	comptime printf("why does this work?\n");
	comptimetest:int32 = comptime fib(3); //comptime 1 + 1;
	printf("comptimetest is:%d\n", comptimetest);
	//comptime 1+1;

	maxerr(2, 3) => catch(e) {
		printf("catched %d\n", e);
	};
	printf("try %d try %d\n", try maxerr(2, 3), try maxerr(3, 2)); //if it fails its UB
	//maxerr(2, 3); //errors
	undefval:int32 = undefined;
	undefstruct:vec2 = undefined;
	//swap(cast(int32 ptr, undefstruct.x), cast(int32 ptr, undefstruct.y));
	undefstruct.x = cast(float, argc);
	printf("%f %f\n", undefstruct.x, undefstruct.y);
	i64v:int64 = 1;
	i32v:int32 = 1;
	i16v:int16 = 1;
	i8v:int8 = 1;
	i1v:bool = 1;
	printf("retundef in main %d\nsimple undef in main %d\n", retundef(), undefval);
	printf("Got %d arguments\n", argc - 1);

	for(i:int32 = 0; i < argc; i +?= 1) {
		printf("Argument %d is:%s\n", i, dereference(argv + i));
		//printf("Argument %d is:%s\n", i, dereference(argv[i]);
	}

	whilei:int32 = 0;
	while(whilei < argc) {
		printf("while Argument %d is:%s\n", whilei, dereference(argv + whilei));
		//printf("Argument %d is:%s\n", whilei, dereference(argv[whilei]);
		whilei +?= 1;
	}

	dowhilei:int32 = 0;
	do {
		printf("dowhile %d\n", dowhilei);
		//dowhilei += 1;
	} while(dowhilei != 0)

	printf("\n");

	printf("%d %d %d %d %d\n", 1 << 2, 1 & 0, 1 | 0, true && false, true || false);

	/*comptime {
		if(asd()) {
			printf("asd returned true!\n");
		}
		else {
			printf("asd returned false?!?!?\n");
		}
	}*/

	//__init__stdio();
	//std.print(cast(char ptr, "test print from struct function\n"));

	a:bool = true;
	a = false;

	//ivec2ptr:ivec2 ptr = cast(ivec2 ptr, stdmalloc(cast(int64, 8)));

	/*NOT implemented yet
	switch(a) {
		case:true {
			//no break;
		}
		case:false {
			//no break;
		}
	}*/

	testarrayanysize1:char[5];
	testarrayanysize2:char[8];
	testarrayanysize(testarrayanysize1); //currently no call
	testarrayanysize(testarrayanysize2); //currently no call
	testarrayanysizenoimplicit(testarrayanysize1); //passes
	//testarrayanysizenoimplicit(testarrayanysize2); //will error

	for(i:int32 = 0; i < 10; i +?= 1) printf("wtf");
	printf("\n");

	for(i:int32 = 0; i < 10; i +?= 1) {
		break;
		printf("after break\n"); //should not print
	}

	printf("should stop at 4\n");
	for(i:int32 = 0; i < 10; i +?= 1) {
		if (i == 5) {
			break;
		}
		printf("%d\n", i);
	}
	printf("\n");

	for(i:int32 = 0; i < 10; i +?= 1) {
		continue;
		printf("after continue\n", i); //should not print
	}

	printf("nested loops breaking at 5\n");
	for(i:int32 = 0; i < 10; i +?= 1) {
		printf("im first\n");
		defer printf("im last here\n\n"); //after i == 5 im last here is not printed, break and continue skips defers
		for(j:int32 = 0; j < 10; j += 1) {
			if(j == 5) {
				printf("i:%d j:%d\n", i, j);
				break;
			}
		}
		if(i == 5) {
			printf("i:%d\n", i);
			break;
		}
	}
	printodd(10);

	defer printf("\ndefer\n");
	defer printf("defer precedence 5+5*5=%d\n", 5 + 5 * 5);
	immuta:immutable int32 = 5; //currently immutable not enforced
	immuta = 6; //invalid

	printf("counting to 10..\n");
	for(i:int32 = 0; i < 10; i +?= 1) {
		printf("%d ", i);
	}
	printf("\n");

	defer {
		defer printf("this will be the last in \"with compound\" and the last in the function too(for now)\n");
		printf("\ndefer\n");
		printf("with\n");
		printf("compound\n");
	};

	printf("should print \"defer in compound\"\n");
	{
		defer printf("defer in compound\n");
	}

	printf("\narray access\n");
	arraysize:int32 = 10;
	a:int32[10];
	for(i:int32 = 0; i < arraysize; i +?= 1) {
		a[i] = i;
	}
	for(i:int32 = 0; i < arraysize; i +?= 1) {
		c:int32 = a[i];
		printf("a[%d] = %d %d\n", i, c, a[i]);
	}
	printf("\n");

	testpointerarith:int32 ptr = addressof(arraysize);
	printf("testpointerarith:%p\n", testpointerarith); //tracking breaks here
	testpointerarith = testpointerarith + 1;
	printf("testpointerarith:%p\n", testpointerarith);
	printf("testpointerarith:%p\n", testpointerarith + 4);

	dummy:int64;
	testpointerarith2:int64 ptr = addressof(dummy);
	printf("testpointerarith2:%p\n", testpointerarith2);
	testpointerarith2 = testpointerarith2 + 1;
	printf("testpointerarith2:%p\n", testpointerarith2);

	arrs:ivec2witharr[2];
	printf("%d %d %d %d %d %d\n", arrs[0].x, arrs[0].y, arrs[1].x, arrs[1].y, arrs[1].z[1].x, arrs[1].z[0].x);
	arrs[0].x = 5;
	arrs[1].z[1].x = 7;
	arrs[1].z[arrs[1].y].x = 8;
	printf("%d %d %d %d %d %d\n", arrs[0].x, arrs[0].y, arrs[1].x, arrs[1].y, arrs[1].z[1].x, arrs[1].z[0].x);

	//printf("%d %f %f\n", testoverloadadd(1, 1), testoverloadadd(1.0, 1.0), testoverloadadd(1.0f, 1.0f));

	testfcf:function (a:int32, b:int32) -> int32 = add;
	print(cast(char ptr, "%d\n"), testfcf(1, 2)); //its calling the print global from std.io which is set to printf
	//there is no implicit casting set on this, so we have to manually cast
	print(cast(char ptr, "addressof print:%p\n\n"), print);

	testfcfanon:function (a:int32, b:int32) -> int32 = function (x:int32, y:int32) -> int32 {
			return x + y;
		};
	printf("%d\n", testfcfanon(1, 2));

	printf("%d\n", firstclassfunctionstest(
		add
	, 1, 2));

	printf("%d\n", firstclassfunctionstest(
		function (x:int32, y:int32) -> int32 {
			return x + y;
		}, 1, 2));

	b:int32;
	printf("zero init:%d\n\n", b);

	a:ivec2;
	a.x = 1;
	a.y = 2;
	printf("struct\n");
	printf("a.x = %d a.y = %d\n", a.x, a.y);
	swap(addressof(a.x), addressof(a.y));
	printf("a.x = %d a.y = %d\n", a.x, a.y);
	printf("\n");

	printf("struct, addressof stored in variable\n");
	xptr:int32 ptr = addressof(a.x);
	yptr:int32 ptr = addressof(a.y);
	printf("a.x = %d a.y = %d\n", dereference(xptr), dereference(yptr));
	swap(xptr, yptr);
	printf("a.x = %d a.y = %d\n", dereference(xptr), dereference(yptr));
	printf("\n");

	printf("struct with polymorphic function\n");
	printf("a.x = %d a.y = %d\n", a.x, a.y);
	//currently not generated
	tswap(addressof(a.x), addressof(a.y));
	printf("a.x = %d a.y = %d\n", a.x, a.y);
	printf("\n");

	x:int32 = 1;
	y:int32 = 2;
	printf("simply variables\n");
	//test implicit cast type checking
	s:char ptr = cast(char ptr, "a = %d b = %d\n");
	printf(s, x, y);
	//printf("a = %d b = %d\n", x, y);
	//currently DOES NOT inline, its in the AST tough
	inline swap(addressof(x), addressof(y));
	printf("a = %d b = %d\n", x, y);
	printf("\n");

	if(1) {
		if(true) {
			return 0;
		}
		printf("end1\n");
		return 0;
	}

	if(false) {
		return 0;
	}

	printf("end\n");

	//typetest(cast(any, 1));
	//typetest(1);
	//typetest(cast(any, 1.0));
	//typetest(cast(any, 1.0f));

	return 0;
}
