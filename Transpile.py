import llvmlite.ir as ir # type: ignore

import AST
import Debug
import Type
from ErrWarnPrint import printWarn, printErr
from copy import deepcopy, copy

class CodegenError(Exception): pass

class CCodeGenerator(object):
	def __init__(self):
		"""Initialize the code generator.

		This creates a new LLVM module into which code is generated. The
		generate_code() method can be called multiple times. It adds the code
		generated for this node into the module, and returns the IR value for
		the node.

		At any time, the current LLVM module being constructed can be obtained
		from the module attribute.
		"""
		# Manages a symbol table while a function is being codegen'd. Maps var
		# names to ir.Value which represents the var's address (alloca).
		self.func_symtab = {}
		# maps func_symtab entries to their ir.type
		#self.type_symtab = {}

		#stores globals to be copied to type_symtab on a new function generation
		self.global_type_symtab = {}
		#holds user defined struct definitions in the type of AST.StructAST
		self.user_types = {}
		#holds the expressions before the final return point of the function
		self.defers = []
		#holds the block of the return point
		self.deferreturnblock = None
		self.DebugInfo = Debug.DummyDebugInfo
		self.isdebug = False
		#tracks the last loop "after" body from which a break statement can get out to
		self.lastLoopAfterBody = None
		#tracks the last loop "loopcmp" body from which a break statement can continue to
		self.lastLoopCmpBody = None
		#track the last loop step expression, for codegen in continue statements
		self.lastLoopStepNode = None
		#holds the body of implicit casts
		self.implicitcasts = {}
		#holds the name by which implicit conversions refer to their argument
		self.implicitcastsname = {}
		#holds the type for implicit conversions
		self.implicitcaststype = {}
		#holds the position for the implicitly converted argument
		self.implicitcastsposition = {}
		self.indent = 0
		self.qualifier_map = {
			'register':     'register',
			'immutable':    'const',
			'thread_local': 'thread_local',
			'atomic':       '_Atomic',
		}

		self.mapback = {
			"double" : "double",
			"float" : "float",
			"none" : "void",
			"any" : "alang_any",
			"T" : "void *",
			"bool" : "bool",
			"char" : "char",
			"int8" : "int8_t",
			"int16" : "int16_t",
			"int32" : "int32_t",
			"int64" : "int64_t",
			"uint8" : "uint8_t",
			"uint16" : "uint16_t",
			"uint32" : "uint32_t",
			"uint64" : "uint64_t",
		}

	def generate_code(self, node):
		assert isinstance(node, (AST.PrototypeAST, AST.FunctionAST, AST.StructAST, AST.NamespaceAST, AST.ImportAST, AST.GlobalVarExprAST, AST.ImplicitCastExpr, AST.ASTNode)) #AST.ASTNode for parsed elems that really return nothing(import if a file was already imported) and its better than none

		#if isinstance(node, AST.PrototypeAST):
		#	return

		return self._codegen(node)

	def _codegen(self, node):
		"""Node visitor. Dispathces upon node type.

		For AST node of class Foo, calls self._codegen_Foo. Each visitor is
		expected to return a llvmlite.ir.Value for REPL.
		"""
		self.DebugInfo.sourceLocation = node.sourceLocation
		method = '_codegen_' + node.__class__.__name__
		return getattr(self, method)(node)

	def maptype(self, typ, varname = ""):
		ret = ""
		for i in typ.qualifiers:
			ret += self.qualifier_map[i] + " "
		if isinstance(typ.type, AST.PrototypeAST):
			args = []
			for i in typ.type.argtypes:
				args.append(self.maptype(i))
			ret += "{0} (*{1})({2})".format(self.maptype(typ.type.rettype), varname, ", ".join(args))
			#ret += "todofirstclassfunctionwithfunctionptr"
		elif typ.type == "va":
			ret += "..."
		elif typ.type in self.mapback:
			ret += self.mapback[typ.type]
		else:
			ret += typ.type

		ret += "*" * typ.indirection
		if typ.isarray:
			#ret += "[{0}]".format(typ.arraysize)
			ret += "*"
		return ret

	def _codegen_ImplicitCastExpr(self, node):
		return ""

	def _codegen_ASTNode(self, node):
		return ""

	def _codegen_ComptimeExprAST(self, node):
		return ""

	def _codegen_UndefinedValueAST(self, node):
		return ""

	def _codegen_SizeofExprAST(self, node):
		ret += "sizeof({0})".format(self._codegen(node));

	def _codegen_ImportAST(self, node):
		ret = ""
		for i in node.ast:
			ret += self._codegen(i)
		return ret

	def _codegen_NamespaceAST(self, node):
		return ""

	def _codegen_DeferExprAST(self, node):
		self.defers.append(node)
		return ""

	def _codegen_StringConstantAST(self, node):
		return "\"{0}\"".format(node.val.replace("\n", "\\n").replace("\"", "\\\""))

	def _codegen_NoopAST(self, node):
		return "1 + 1"
		#ret += "1 + 1"

	def _codegen_StructAST(self, node):
		ret = ""
		ret += "typedef struct {0} {{\n".format(node.name)
		for i in range(len(node.fieldnames)):
			if isinstance(node.fieldtypes[i].type, AST.PrototypeAST):
				ret += "\t{0};\n".format(self.maptype(node.fieldtypes[i], node.fieldnames[i]))
			else:
				ret += "\t{0} {1};\n".format(self.maptype(node.fieldtypes[i]), node.fieldnames[i]);
		ret += "}} {0};\n\n".format(node.name)
		return ret

	def _codegen_MemberExprAST(self, node):
		return "{0}.{1}".format(self._codegen(node.var), node.fieldname);

	def _codegen_ArrayAccessExprAST(self, node, noload = False):
		return "{0}[{1}]".format(self._codegen(node.variable), self._codegen(node.element))

	def _codegen_BooleanExprAST(self, node):
		if node.val == 0:
			return "false"
		else:
			return "true"

	def _codegen_TypeofExprAST(self, node):
		return ""

	def _codegen_DereferenceExprAST(self, node):
		return "*({0})".format(self._codegen(node.expr))

	def _codegen_AddressofExprAST(self, node):
		return "&{0}".format(self._codegen(node.expr))

	def _codegen_CastExprAST(self, node):
		return "({0})({1})".format(self.maptype(node.type), self._codegen(node.expr))

	def _codegen_NumberExprAST(self, node):
		return "{0}".format(node.val)

	def _codegen_VariableExprAST(self, node, noload = False):
		return "{0}".format(node.name)

	def _codegen_UnaryExprAST(self, node):
		return "{0}({1})".format('unary{0}'.format(node.op), self._codegen(node.operand))

	def _codegen_ReturnExprAST(self, node):
		if not isinstance(node.body, AST.NoopAST):
			return "return {0}".format(self._codegen(node.body))
		else:
			return "return"

	def _codegen_BinaryExprAST(self, node):
		#FIXME implement ? operators
		return "{0} {1} {2}".format(self._codegen(node.lhs), node.op.replace("?", ""), self._codegen(node.rhs))

	def _codegen_IfExprAST(self, node):
		ret = ""
		ret += "if({0}) {1}".format(self._codegen(node.cond_expr), self._codegen(node.then_expr))
		if node.has_else:
			ret += "\t" * (self.indent) + "else {0}".format(self._codegen(node.else_expr))
		return ret

	def _codegen_CompoundStatementAST(self, node):
		docurlyindent = True
		if docurlyindent:
			ret = "\t" * self.indent + "{\n"
		self.indent += 1
		for expression in node.statements:
			ret += "{0}{1}".format("\t" * self.indent, self._codegen(expression))
			if not isinstance(expression, (AST.ForExprAST, AST.WhileExprAST, AST.DoWhileExprAST, AST.IfExprAST, AST.DeferExprAST, AST.ComptimeExprAST)):
				ret += ";"
			ret += "\n"
		self.indent -= 1

		if docurlyindent:
			ret += "\t" * self.indent + "}\n"

		return ret


	def _codegen_BreakExprAST(self, node):
		return "break"

	def _codegen_ContinueExprAST(self, node):
		return "continue"

	def _codegen_ForExprAST(self, node):
		return "for({0};{1};{2}) {3}".format(self._codegen(node.start_expr), self._codegen(node.end_expr), self._codegen(node.step_expr), self._codegen(node.body))

	def _codegen_WhileExprAST(self, node):
		return "while({0}) {1}".format(self._codegen(node.end_expr), self._codegen(node.body))

	def _codegen_DoWhileExprAST(self, node):
		return "do {1} while({0});".format(self._codegen(node.end_expr), self._codegen(node.body))

	def _codegen_GlobalVarExprAST(self, node):
		ret = ""
		if isinstance(node.init, AST.UndefinedValueAST):
			ret += "{0} {1};\n".format(self.maptype(node.id_type), node.name)
		elif node.init is None:
			ret += "{0} {1} = 0;\n".format(self.maptype(node.id_type), node.name)
		else:
			ret += "{0} {1} = {2};\n".format(self.maptype(node.id_type), node.name, self._codegen(node.init))
		self.global_type_symtab[node.name] = node.id_type
		return ret

	def _codegen_VarExprAST(self, node):
		ret = ""
		if node.name in self.func_symtab:
			printWarn("Redefinition of \"{0}\" shadows a previous decleration".format(node.name))

		if node.init is not None:
			ret += "{0} {1} = {2}".format(self.maptype(node.id_type), node.name, self._codegen(node.init))
		else:
			ret += "{0} {1} = 0".format(self.maptype(node.id_type), node.name)
		return ret

	def _codegen_polymorphic_call(self, call, node):
		#print(node.dump())
		if call.callee not in self.generated_polymorphicfunctions:
			self.generated_polymorphicfunctions[call.callee] = {}
		print(self.generated_polymorphicfunctions)
		printErr("Polymorphic function calls not yet implemented, no calls emitted")
		return ""

	def _codegen_CallExprAST(self, node, tail = False):
		ret = ""
		ret += "{0}(".format(node.callee)
		for i in range(len(node.args)):
			ret += self._codegen(node.args[i])
			if i < len(node.args) - 1:
				ret += ", "
		ret += ")"
		return ret

	def _codegen_PrototypeAST(self, node, isdeclaration = False):
		ret = ""
		ret += "{0} {1}(".format(self.maptype(node.rettype), node.name)
		for i in range(len(node.argnames)):
			if isinstance(node.argtypes[i].type, AST.PrototypeAST):
				ret += "{0}".format(self.maptype(node.argtypes[i], node.argnames[i]))
			else:
				ret += "{0} {1}".format(self.maptype(node.argtypes[i]), node.argnames[i])
			if i < len(node.argnames) - 1:
				ret += ", "
		ret += ");\n"
		return ret

	def _codegen_FunctionAST(self, node, cont = True):
		if node.is_anonymous():
			return node.proto.name

		funcrettype = ""
		if node.proto.throws:
			funcrettype = "struct {{bool success; union {{ int errorcode; {0}}}}}".format(self.maptype(node.proto.rettype))
		else:
			funcrettype = self.maptype(node.proto.rettype)
		ret = ""
		ret += "{0} {1}(".format(funcrettype, node.proto.name)
		for i in range(len(node.proto.argnames)):
			if isinstance(node.proto.argtypes[i].type, AST.PrototypeAST):
				ret += "{0}".format(self.maptype(node.proto.argtypes[i], node.proto.argnames[i]))
			else:
				ret += "{0} {1}".format(self.maptype(node.proto.argtypes[i]), node.proto.argnames[i])
			if i < len(node.proto.argnames) - 1:
				ret += ", "
		if isinstance(node.body, AST.CompoundStatementAST):
			ret += ") {0}\n".format(self._codegen(node.body))
		else:
			ret += ") {{\n\t{0};\n}}\n\n".format(self._codegen(node.body))
		return ret

	def _codegen_ThrowExprAST(self, node):
		ret = ""
		ret += "return (struct {bool success; union { int errorcode; }}){.success = false, .errorcode = 1}"
		return ret

		#printErr("throw not implemented")
		#pass

	def _codegen_TryExprAST(self, node):
		return self._codegen(node.body) + ".value"

	def _codegen_CatchExprAST(self, node):
		return self._codegen(node.body)
