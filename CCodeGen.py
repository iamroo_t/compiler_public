import llvmlite.ir as ir # type: ignore

import AST
import Debug
from ErrWarnPrint import printWarn, printErr
from copy import deepcopy

class CodegenError(Exception): pass

class LLVMCodeGenerator(object):
	def __init__(self):
		"""Initialize the code generator.

		This creates a new LLVM module into which code is generated. The
		generate_code() method can be called multiple times. It adds the code
		generated for this node into the module, and returns the IR value for
		the node.

		At any time, the current LLVM module being constructed can be obtained
		from the module attribute.
		"""
		self.module = ir.Module()
		#counter for strings, to emit a unique name for them as str.NUMBER
		self.str_count = 0

		# Current IR builder.
		self.builder = ir.IRBuilder()

		self.targetinfo = None
		# Manages a symbol table while a function is being codegen'd. Maps var
		# names to ir.Value which represents the var's address (alloca).
		self.func_symtab = {}
		# maps func_symtab entries to their ir.type
		self.type_symtab = {}
		#stores globals to be copied to type_symtab on a new function generation
		self.global_type_symtab = {}
		#holds user defined struct definitions in the type of AST.StructAST
		self.user_types = {}
		#holds the expressions before the final return point of the function
		self.defers = []
		#holds the block of the return point
		self.deferreturnblock = None
		self.DebugInfo = Debug.DummyDebugInfo
		self.isdebug = False
		#tracks the last loop "after" body from which a break statement can get out to
		self.lastLoopAfterBody = None
		#tracks the last loop "loopcmp" body from which a break statement can continue to
		self.lastLoopCmpBody = None
		#holds the body of implicit casts
		self.implicitcasts = {}
		#holds the name by which implicit conversions refer to their argument
		self.implicitcastsname = {}
		#holds the type for implicit conversions
		self.implicitcaststype = {}
		#holds the position for the implicitly converted argument
		self.implicitcastsposition = {}
		#holds the list of polymorphic functions
		self.polymorphicfunctions = {}
		self.generated_polymorphicfunctions = {}


	def generate_code(self, node):
		assert isinstance(node, (AST.PrototypeAST, AST.FunctionAST, AST.StructAST, AST.NamespaceAST, AST.ImportAST, AST.GlobalVarExprAST, AST.ImplicitCastExpr))

		#if isinstance(node, AST.PrototypeAST):
		#	return

		return self._codegen(node)

	def _debug_emit_alloca(self, name, alloca, builder):
		if "llvm.dbg.declare" not in self.module.globals and self.isdebug:
			dbgty = ir.FunctionType(ir.VoidType(), [ir.MetaDataType()] * 3)
			self.DebugInfo.dbgDeclareFunc = ir.Function(self.module, dbgty, "llvm.dbg.declare")
			self.DebugInfo.dbgAddrFunc = ir.Function(self.module, dbgty, "llvm.dbg.addr")
			self.DebugInfo.dbgValueFunc = ir.Function(self.module, dbgty, "llvm.dbg.value")

		if self.isdebug:
			builder.call(self.DebugInfo.dbgDeclareFunc, [
				alloca,
				self.DebugInfo.local_variable(self.module, name, self.DebugInfo.variable_type(self.module, "double", 8)),
				self.DebugInfo.expression(self.module)])

	def _create_entry_block_alloca(self, name, type):
		"""Create an alloca in the entry BB of the current function."""
		builder = ir.IRBuilder()
		builder.position_at_start(self.builder.function.entry_basic_block)
		#typ = None#self.DebugInfo.variable_type(self.module, "float", 32)
		#builder.call(llvm.dbg.declare)
		#builder.debug_metadata = self.DebugInfo.local_variable(self.module, name, type)
		builder.debug_metadata = self.DebugInfo.location(self.module)
		ret = builder.alloca(type, size=None, name=name)
		#FIXME local_variable and variable_type
		self._debug_emit_alloca(name, ret, builder)
		return ret

	def _codegen(self, node, builder = None):
		"""Node visitor. Dispathces upon node type.

		For AST node of class Foo, calls self._codegen_Foo. Each visitor is
		expected to return a llvmlite.ir.Value.
		"""
		self.DebugInfo.sourceLocation = node.sourceLocation
		usedbuilder = self.builder
		#if builder is not self._codegen.__defaults__[0]:
		if builder != None:
			usedbuilder = builder
		method = '_codegen_' + node.__class__.__name__
		return getattr(self, method)(node, usedbuilder)

	def _codegen_SizeofExprAST(self, node, builder):
		print("sizeof:{0} is {1}".format(str(node.irtype), node.irtype.get_abi_size(self.targetinfo)))

	def _codegen_IntrinsicExprAST(self, node, builder):
		if node.intrinsic == "asm":
			#thorws: LLVM ERROR: Inline asm not supported by this streamer because we don't have an asm parser for this target, so I guess no then
			#fty = ir.FunctionType(ir.VoidType(), [])
			#return builder.asm(fty, node.exprs[0].val, "", [], False)#builder.call(, [])
			raise NotImplementedError
		else:
			raise CodegenError('Unknown intrinsic function ' + str(node.intrinsic))

	def _codegen_ImplicitCastExpr(self, node, builder):
		if node.function not in self.implicitcasts:
			self.implicitcasts[node.function] = []
		self.implicitcasts[node.function].append(node.body) #self._codegen(node.body, builder)
		self.implicitcastsname[node.function] = node.argname
		if node.function not in self.implicitcaststype:
			self.implicitcaststype[node.function] = []
		self.implicitcaststype[node.function].append(node.irtype)
		if node.function not in self.implicitcastsposition:
			self.implicitcastsposition[node.function] = []
		self.implicitcastsposition[node.function].append(node.argcount)

	def _codegen_ImportAST(self, node, builder):
		for i in node.ast:
			self._codegen(i, builder)
		return

	def _codegen_NamespaceAST(self, node, builder):
		return

	def _codegen_DeferExprAST(self, node, builder):
		self.defers.append(node)

	def _codegen_StringConstantAST(self, node, builder):
		r = ir.GlobalVariable(self.module, ir.ArrayType(ir.IntType(8), len(node.val) + 1), ".str" + str(self.str_count))
		self.str_count += 1
		r.linkage = 'internal'
		r.global_constant = True
		arg = str(node.val + '\0')
		r.initializer = ir.Constant(ir.ArrayType(ir.IntType(8), len(node.val) + 1), bytearray(arg.encode("utf8")))
		r.deduplicate_name = True
		return r

	def _codegen_NoopAST(self, node, builder):
		return builder.add(ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), 0))

	def _codegen_StructAST(self, node, builder):
		self.user_types[node.name] = node
		return node.irtype.get_declaration()

	def _codegen_MemberExprAST(self, node, builder, noload=False):
		if isinstance(node.var, AST.VariableExprAST):
			n = self._codegen_VariableExprAST(node.var, builder, True)
			if node.var.name in self.type_symtab:
				of = self.user_types[self.type_symtab[node.var.name].type].fieldnames.index(node.fieldname)
			elif node.var.name in self.global_type_symtab:
				of = self.user_types[self.global_type_symtab[node.var.name].type].fieldnames.index(node.fieldname)
			else:
				raise NotImplementedError
		elif isinstance(node.var, AST.ArrayAccessExprAST):
			n = self._codegen_ArrayAccessExprAST(node.var, builder, True)
			of = self.user_types[node.var.irtype.name].fieldnames.index(node.fieldname)
		else:
			raise NotImplementedError

		offsets = [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), of)]

		builder.debug_metadata = self.DebugInfo.location(self.module)
		ret = builder.gep(n, offsets, True)
		if noload:
			return ret
		return builder.load(ret)

	def _codegen_ArrayAccessExprAST(self, node, builder, noload = False):
		offsets = [ir.Constant(ir.IntType(32), 0), self._codegen(node.element, builder)]
		builder.debug_metadata = self.DebugInfo.location(self.module)
		if isinstance(node.variable, AST.VariableExprAST):
			n = self._codegen_VariableExprAST(node.variable, builder, True)
		elif isinstance(node.variable, AST.MemberExprAST):
			n = self._codegen_MemberExprAST(node.variable, builder, True)
		else:
			raise NotImplementedError

		ret = builder.gep(n, offsets, True)

		if noload:
			return ret
		return builder.load(ret)

	def _codegen_BooleanExprAST(self, node, builder):
		return ir.Constant(node.irtype, int(node.val))

	def _codegen_TypeofExprAST(self, node, builder):
		builder.debug_metadata = self.DebugInfo.location(self.module)
		typestr = str(node.irtype)
		#node.irtype = ir.ArrayType(ir.IntType(8), len(typestr) + 1)
		#FIXME should return a variable to be cast-able
		alloca = builder.alloca(ir.ArrayType(ir.IntType(8), len(typestr) + 1), size=None, name="typeof")
		#FIXME "typeof" is not unique
		self._debug_emit_alloca("typeof", alloca, builder)
		storeval = ir.Constant(ir.ArrayType(ir.IntType(8), len(typestr) + 1), bytearray(typestr.encode("utf8")))
		builder.store(storeval, alloca)
		#return builder.load(alloca)
		return alloca
		#return builder.load(self._codegen(node.expr))

	def _codegen_DereferenceExprAST(self, node, builder):
		builder.debug_metadata = self.DebugInfo.location(self.module)
		return builder.load(self._codegen(node.expr, builder))

	def _codegen_AddressofExprAST(self, node, builder):
		assert(isinstance(node.expr, (AST.VariableExprAST, AST.ArrayAccessExprAST, AST.MemberExprAST, AST.CastExprAST)))
		builder.debug_metadata = self.DebugInfo.location(self.module)
		if isinstance(node.expr, AST.VariableExprAST):
			var_addr = self._codegen_VariableExprAST(node.expr, builder, True)
		elif isinstance(node.expr, AST.ArrayAccessExprAST):
			var_addr = self._codegen_ArrayAccessExprAST(node.expr, builder, True)
		elif isinstance(node.expr, AST.MemberExprAST):
			var_addr = self._codegen_MemberExprAST(node.expr, builder, True)
		elif isinstance(node.expr, AST.CastExprAST):
			return self._codegen_AddressofExprAST(self._codegen(node.expr, builder))
		return var_addr

	def _codegen_CastExprAST(self, node, builder):
		fromtype = node.expr.irtype
		totype = node.irtype
		builder.debug_metadata = self.DebugInfo.location(self.module)
		if isinstance(fromtype, ir.ArrayType) and totype.is_pointer:
			a = self._codegen(node.expr, builder)
			of = [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), 0)]
			return builder.gep(a, of, True) #builder.bitcast(builder.gep(a, of, True), totype)
		elif not totype.is_pointer and not fromtype.is_pointer:
			if isinstance(fromtype, (ir.FloatType, ir.DoubleType)) and isinstance(totype, ir.IntType):
				return builder.fptosi(self._codegen(node.expr, builder), totype)
			elif isinstance(fromtype, ir.IntType) and isinstance(totype, (ir.FloatType, ir.DoubleType)):
				return builder.sitofp(self._codegen(node.expr, builder), totype)
			elif isinstance(fromtype, ir.DoubleType) and isinstance(totype, ir.FloatType):
				return builder.fptrunc(self._codegen(node.expr, builder), totype)
			elif isinstance(fromtype, ir.FloatType) and isinstance(totype, ir.DoubleType):
				return builder.fpext(self._codegen(node.expr, builder), totype)
			elif isinstance(totype, ir.IntType) and isinstance(fromtype, ir.IntType):
				if fromtype.width > totype.width:
					return builder.trunc(self._codegen(node.expr, builder), totype)
				elif fromtype.width < totype.width:
					return builder.sext(self._codegen(node.expr, builder), totype)
			else:
				raise CodegenError('No viable conversion from {0} to {1}'.format(fromtype, totype))
		elif totype.is_pointer and fromtype.is_pointer:
			return builder.bitcast(self._codegen(node.expr, builder), totype)

		else:
			print(totype)
			print(fromtype)
			print(totype.is_pointer)
			print(fromtype.is_pointer)
			raise CodegenError('No viable conversion from {0} to {1}'.format(fromtype, totype))
		"""elif totype == fromtype:
			print(totype._get_ll_pointer_type(None))
			if totype.indirection > fromtype.indirection:
				x = totype.indirection - fromtype.indirection
				ret = totype
				while x != 0:
					ret = ret.as_pointer()
					x -= 1
				return ret"""

	def _codegen_NumberExprAST(self, node, builder):
		val = node.val
		if node.type.type == "float" or node.type.type == "double":
			val = float(node.val.replace("f", ""))
		else:
			val = int(node.val)
		return ir.Constant(node.irtype, val)

	def _codegen_VariableExprAST(self, node, builder, noload = False):
		try:
			var_addr = self.func_symtab[node.name]
		except:
			try:
				var_addr = self.module.get_global(node.name)
				return var_addr
			except:
				raise CodegenError("reference to undefined non extern variable")
		if noload:
			return var_addr
		builder.debug_metadata = self.DebugInfo.location(self.module)
		ret = builder.load(var_addr, node.name, align=4)
		return ret

	def _codegen_UnaryExprAST(self, node, builder):
		'''C STYLE addressof/dereference, maybe has parsing problems
		if node.op == "*":
			return self._codegen_DereferenceExprAST(AST.DereferenceExprAST(node.operand), builder)
		if node.op == "&":
			return self._codegen_AddressofExprAST(AST.AddressofExprAST(node.operand), builder)'''
		builder.debug_metadata = self.DebugInfo.location(self.module)
		operand = self._codegen(node.operand, builder)
		func = self.module.get_global('unary{0}'.format(node.op))
		return builder.call(func, [operand], 'unop')

	def _codegen_ReturnExprAST(self, node, builder):
		builder.debug_metadata = self.DebugInfo.location(self.module)
		if isinstance(node.body, AST.CallExprAST):
			c = self._codegen_CallExprAST(node.body, builder, True)
		else:
			c = self._codegen(node.body, builder)
		if not isinstance(node.body, AST.NoopAST):
			ret = builder.store(c, self.func_symtab["returnvalue"])
		builder.branch(self.deferreturnblock)
		if not isinstance(node.body, AST.NoopAST):
			return ret

	def _codegen_BinaryExprAST(self, node, builder):
		# Assignment is handled specially because it doesn't follow the general
		# recipe of binary ops.
		if node.op == '=':
			if not isinstance(node.lhs, (AST.VariableExprAST, AST.MemberExprAST, AST.DereferenceExprAST, AST.ArrayAccessExprAST, AST.UnaryExprAST)):
				raise CodegenError('left hand side of assignement must be a variable')
			if isinstance(node.lhs, AST.VariableExprAST):
				var_addr = self._codegen_VariableExprAST(node.lhs, builder, True)
			elif isinstance(node.lhs, AST.DereferenceExprAST):
				var_addr = self._codegen(node.lhs.expr, builder)
			elif isinstance(node.lhs, AST.ArrayAccessExprAST):
				var_addr = self._codegen_ArrayAccessExprAST(node.lhs, builder, True)
			elif isinstance(node.lhs, AST.MemberExprAST):
				var_addr = self._codegen_MemberExprAST(node.lhs, builder, True)
			else:
				var_addr = self._codegen(node.lhs, builder)

			if isinstance(node.rhs, AST.MemberExprAST):
				rhs_val = self._codegen(node.rhs, builder)
			else:
				rhs_val = self._codegen(node.rhs, builder)
			#var_addr = self._codegen(node.lhs, builder)
			#rhs_val = self._codegen(node.rhs, builder)
			builder.debug_metadata = self.DebugInfo.location(self.module)
			builder.store(rhs_val, var_addr, align=4)
			return rhs_val

		lhs = self._codegen(node.lhs, builder)
		rhs = self._codegen(node.rhs, builder)

		builder.debug_metadata = self.DebugInfo.location(self.module)

		if isinstance(node.lhs.irtype, (ir.FloatType, ir.DoubleType)):
			if node.op == '+':
				return builder.fadd(lhs, rhs, 'addtmp')
			elif node.op == '-':
				return builder.fsub(lhs, rhs, 'subtmp')
			elif node.op == '*':
				return builder.fmul(lhs, rhs, 'multmp')
			elif node.op == '<':
				cmp = builder.fcmp_unordered('<', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '<=':
				cmp = builder.fcmp_unordered('<=', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '>':
				cmp = builder.fcmp_unordered('>', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '>=':
				cmp = builder.fcmp_unordered('>=', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '==':
				cmp = builder.fcmp_unordered('==', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '!=':
				cmp = builder.fcmp_unordered('!=', lhs, rhs, 'cmptmp')
				return builder.uitofp(cmp, node.lhs.irtype, 'booltmp')
			elif node.op == '%':
				return builder.frem(node.lhs, node.rhs, "fremtmp")
			else:
				# None one of predefined operators, so it must be a user-defined one.
				# Emit a call to it.
				func = self.module.get_global('binary{0}'.format(node.op))
				return builder.call(func, [lhs, rhs], 'binop')

		#must be integer
		elif isinstance(node.lhs.irtype, ir.IntType):
			if node.op == '+':
				return builder.add(lhs, rhs, 'addtmp')
			elif node.op == '-':
				return builder.sub(lhs, rhs, 'subtmp')
			elif node.op == '*':
				return builder.mul(lhs, rhs, 'multmp')
			elif node.op == '<':
				return builder.icmp_signed('<', lhs, rhs, 'cmptmp')
			elif node.op == '>':
				return builder.icmp_signed('>', lhs, rhs, 'cmptmp')
			elif node.op == '<=':
				return builder.icmp_signed('<=', lhs, rhs, 'cmptmp')
			elif node.op == '>=':
				return builder.icmp_signed('>=', lhs, rhs, 'cmptmp')
			elif node.op == '==':
				return builder.icmp_signed('==', lhs, rhs, 'cmptmp')
			elif node.op == '!=':
				return builder.icmp_signed('!=', lhs, rhs, 'cmptmp')
			elif node.op == '%':
				return builder.srem(lhs, rhs, 'sremtmp')
			else:
				# None one of predefined operators, so it must be a user-defined one.
				# Emit a call to it.
				func = self.module.get_global('binary{0}'.format(node.op))
				return builder.call(func, [lhs, rhs], 'binop')
		elif isinstance(node.lhs, AST.VariableExprAST) and isinstance(node.lhs.irtype, ir.PointerType):
			return builder.gep(lhs, [rhs], True)
		print(node.lhs.irtype)
		print(node.rhs.irtype)
		print(node.dump())
		raise NotImplementedError

	def _codegen_IfExprAST(self, node, builder):
		# Emit comparison value
		cond_val = self._codegen(node.cond_expr, builder)
		if isinstance(node.cond_expr.irtype, (ir.FloatType, ir.DoubleType)):
			cmp = builder.fcmp_ordered('!=', cond_val, ir.Constant(ir.DoubleType(), 0.0))
		else:
			cmp = builder.icmp_signed('!=', cond_val, ir.Constant(ir.IntType(32), 0))

		# Create basic blocks to express the control flow, with a conditional
		# branch to either then_bb or else_bb depending on cmp. else_bb and
		# merge_bb are not yet attached to the function's list of BBs because
		# if a nested IfExpr is generated we want to have a reasonably nested
		# order of BBs generated into the function.
		then_bb = builder.function.append_basic_block('then')
		else_bb = ir.Block(builder.function, 'else')
		merge_bb = ir.Block(builder.function, 'ifcont')

		builder.cbranch(cmp, then_bb, else_bb)

		# Emit the 'then' part
		builder.position_at_start(then_bb)
		then_val = self._codegen(node.then_expr, builder)
		if not builder.block.is_terminated:
			builder.branch(merge_bb)

		# remember which block the 'then' part ends in.
		then_bb = builder.block

		# Emit the 'else' part
		builder.function.basic_blocks.append(else_bb)
		builder.position_at_start(else_bb)
		else_val = self._codegen(node.else_expr, builder)

		# Emission of else_val could have modified the current basic block.
		else_bb = builder.block
		#if not builder.block.is_terminated:
		builder.branch(merge_bb)

		# Emit the merge ('ifcnt') block only if none of the branches returned so the else_bb branch can jump into it
		builder.function.basic_blocks.append(merge_bb)
		builder.position_at_start(merge_bb)

		return then_val #just return something


	def _codegen_CompoundStatementAST(self, node, builder, isfirst=False):
		ret = ""
		if not isfirst:
			saved_debuginfo_scope = self.DebugInfo.current_scope
			#FIXME at Parser.py:417 it reports correct line column info, at here its the position at the first expression after the compound statement
			#print("DI Lexical {0} {1}".format(self.DebugInfo.sourceLocation.line, self.DebugInfo.sourceLocation.column))
			#print("node Lexical {0} {1}".format(node.sourceLocation.line, node.sourceLocation.column))
			builder.debug_metadata = self.DebugInfo.location(self.module)
			self.DebugInfo.lexicalblock(self.module)
		for expression in node.statements:
			if isinstance(expression, (AST.BreakExprAST, AST.ContinueExprAST)):
				self._codegen(expression, builder)
				break #should be dead code after this, might as well not emit it
			ret = self._codegen(expression, builder)
		if not isfirst:
			self.DebugInfo.current_scope = saved_debuginfo_scope

		return ret

	def _codegen_BreakExprAST(self, node, builder):
		#printErr("break expressions not yet implemented, loop will continue as normal")
		#pass
		#break_bb = builder.function.append_basic_block('break')
		builder.branch(self.lastLoopAfterBody)
		#builder.position_at_start(break_bb)
		#builder.branch(break_bb)

	def _codegen_ContinueExprAST(self, node, builder):
		#printErr("continue expressions not yet implemented, loop will continue as normal")
		#continue_bb = builder.function.append_basic_block('continue')
		builder.branch(self.lastLoopCmpBody)
		#builder.position_at_start(continue_bb)
		#builder.branch(continue_bb)

	def _codegen_ForExprAST(self, node, builder):
		# Output this as:
		#   var = alloca double
		#   ...
		#   start = startexpr
		#   store start -> var
		#   goto loop
		# loop:
		#   ...
		#   bodyexpr
		#   ...
		# loopend:
		#   step = stepexpr
		#   endcond = endexpr
		#   curvar = load var
		#   nextvariable = curvar + step
		#   store nextvar -> var
		#   br endcond, loop, afterloop
		# afterloop:

		# Emit the start expr first, without the variable in scope. Store it
		# into the var.
		if not isinstance(node.start_expr, AST.NoopAST):
			self._codegen(node.start_expr, builder)
			start_val = self.func_symtab[node.start_expr.name]

		loop_bb = builder.function.append_basic_block('loop')
		loop_cmp = builder.function.append_basic_block('loopcmp')
		# Create the 'after loop' block and insert it
		after_bb = builder.function.append_basic_block('afterloop')
		savedLastLoopAfterBody = self.lastLoopAfterBody
		self.lastLoopAfterBody = after_bb
		savedLastLoopCmpBody = self.lastLoopCmpBody
		self.lastLoopCmpBody = loop_cmp
		# Insert the conditional branch into the end of loop_end_bb

		# Insert an explicit fall through from the current block to loop_cmp
		builder.branch(loop_cmp)
		builder.position_at_start(loop_cmp)

		# Compute the end condition
		endcond = ir.Constant(ir.IntType(1), 1)
		if not isinstance(node.end_expr, AST.NoopAST):
			endcond = self._codegen(node.end_expr, builder)

		#insert the exit or re run conditional jump
		builder.cbranch(endcond, loop_bb, after_bb)

		#the loop body
		builder.position_at_start(loop_bb)

		if isinstance(node.step_expr, AST.NoopAST):
			stepval = ir.Constant(ir.IntType(8), 1)
		else:
			stepval = self._codegen(node.step_expr, builder)

		if not isinstance(node.start_expr, AST.NoopAST):
			cur_var = builder.load(start_val)

		# Emit the body of the loop. This, like any other expr, can change the
		# current BB. Note that we ignore the value computed by the body.
		body_val = self._codegen(node.body, builder)

		#another round?
		if not builder.block.is_terminated:
			builder.branch(loop_cmp)

		# New code will be inserted into after_bb
		builder.position_at_start(after_bb)
		self.lastLoopAfterBody = savedLastLoopAfterBody
		self.lastLoopCmpBody = savedLastLoopCmpBody

		#delete the loop created var, if one was defined
		if not isinstance(node.start_expr, AST.NoopAST):
			del self.func_symtab[node.start_expr.name]

	def _codegen_GlobalVarExprAST(self, node, builder):
		old_bindings = []
		if node.name in self.module.globals:
			raise CodegenError("redefinition of global variable/function: {0}".format(node.name))
		# Emit the initializer before adding the variable to scope. This
		# prevents the initializer from referencing the variable itself.
		if node.init is not None:
			init_val = self._codegen(node.init, builder)
		else:
			init_val = ir.Constant(node.irtype, None) #None is LLVM's zero initializer

		#builder.debug_metadata = self.DebugInfo.location(self.module)
		var_addr = ir.GlobalVariable(self.module, node.irtype, node.name)
		var_addr.initializer = init_val
		if node.linkage != "external":
			toset = self.module.get_global(node.name)
			toset.linkage = node.linkage
		#builder.store(init_val, var_addr)

		self.global_type_symtab[node.name] = node.id_type

	def _codegen_VarExprAST(self, node, builder):
		old_bindings = []
		if node.name in self.func_symtab:
			printWarn("Redefinition of \"{0}\" shadows a previous decleration".format(node.name))
		# Emit the initializer before adding the variable to scope. This
		# prevents the initializer from referencing the variable itself.
		if node.init is not None:
			init_val = self._codegen(node.init, builder)
		else:
			init_val = ir.Constant(node.irtype, None) #None is LLVM's zero initializer

		# Create an alloca for the induction var and store the init value to
		# it. Save and restore location of our builder because
		# _create_entry_block_alloca may modify it (llvmlite issue #44).
		saved_block = builder.block
		var_addr = self._create_entry_block_alloca(node.name, node.irtype)
		builder.position_at_end(saved_block)
		builder.store(init_val, var_addr)

		# We're going to shadow this name in the symbol table now; remember
		# what to restore.
		old_bindings.append(self.func_symtab.get(node.name))
		self.func_symtab[node.name] = var_addr
		self.type_symtab[node.name] = node.id_type

	def _codegen_polymorphic_call(self, call, node, builder):
		#print(node.dump())
		if call.callee not in self.generated_polymorphicfunctions:
			self.generated_polymorphicfunctions[call.callee] = {}
		print(self.generated_polymorphicfunctions)
		printErr("Polymorphic function calls not yet implemented, no calls emitted")
		return None

	def _codegen_CallExprAST(self, node, builder, tail = False):
		if isinstance(node.callee, AST.ASTNode):
			callee_func = self._codegen(node.callee, builder)
			call_args = [self._codegen(arg, builder) for arg in node.args]
			ret = builder.call(callee_func, call_args, 'calltmp', tail=tail)
			return ret
		else:
			try:
				callee_func = self.module.get_global(node.callee)
			except:
				try:
					callee_func = builder.load(self.func_symtab[node.callee])
					call_args = [self._codegen(arg, builder) for arg in node.args]
					builder.debug_metadata = self.DebugInfo.location(self.module)
					ret = builder.call(callee_func, call_args, 'calltmp', tail=tail)
					return ret
				except:
					try:
						body = deepcopy(self.polymorphicfunctions[node.callee])
						return self._codegen_polymorphic_call(node, body, builder)
					except KeyError:
						raise CodegenError('Call to unknown function: ' + str(node.callee))
		if not isinstance(callee_func, ir.Function):
			call_args = [self._codegen(arg, builder) for arg in node.args]
			builder.debug_metadata = self.DebugInfo.location(self.module)
			ret = builder.call(builder.load(callee_func), call_args, 'calltmp', tail=tail)
			return ret
		if not callee_func.function_type.var_arg and len(callee_func.args) != len(node.args):
			raise CodegenError('Call argument length mismatch', node.callee)
		call_args = [self._codegen(arg, builder) for arg in node.args]
		if node.callee in self.implicitcasts:
			ii = 0
			for i in self.implicitcastsposition[node.callee]:
				if str(self.implicitcaststype[node.callee][ii]) == str(node.args[i].irtype) or (isinstance(node.args[i].irtype, ir.ArrayType) and self.implicitcaststype[node.callee][ii].count == 0):
					self.func_symtab[self.implicitcastsname[node.callee]] = call_args[i]
					r = self._codegen(self.implicitcasts[node.callee][ii], builder)
					del self.func_symtab[self.implicitcastsname[node.callee]]
					call_args[i] = r
					break
				ii += 1

		for i, arg in enumerate(callee_func.args):
			if isinstance(call_args[i].type, ir.ArrayType) and isinstance(arg.type, ir.ArrayType) and arg.type.count == 0:
				asd = builder.gep(call_args[i].operands[0], [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), 0)])
				#asd = builder.bitcast(call_args[i], arg.type)
				call_args[i] = asd
				printErr("implicit array conversions not yet implemented, no calls emitted")
				return
		ret = builder.call(callee_func, call_args, 'calltmp', tail=tail)
		return ret

	def _codegen_PrototypeAST(self, node, builder):
		func_ty = node.irtype
		# If a function with this name already exists in the module...
		if node.name in self.module.globals:
			# We only allow the case in which a declaration exists and now the
			# function is defined (or redeclared) with the same number of args.
			#existing_func = self.module[funcname]
			existing_func = self.module.get_global(node.name)
			if not isinstance(existing_func, ir.Function):
				raise CodegenError('Function/Global name collision', node.name)
			if not existing_func.is_declaration:
				raise CodegenError('Redifinition of {0}'.format(node.name))
			if len(existing_func.function_type.args) != len(func_ty.args):
				raise CodegenError(
					'Redifinition with different number of arguments')
			func = self.module.globals[node.name]
		else:
			# Otherwise create a new function
			func = ir.Function(self.module, func_ty, node.name)
		return func

	def _codegen_FunctionAST(self, node, builder, cont = True):
		if node.proto.ispolymorphic:
			self.polymorphicfunctions[node.proto.name] = node.body
			return

		if builder == None:
			self.builder = ir.IRBuilder()
		else:
			self.builder = builder

		if node.is_anonymous() and cont:
			#save the builder for the current block we ware at becuase the anonymous function generation will reposition the builder, also ssave this functions locals, defers, exit block
			saved_builder = self.builder
			saved_deferreturnblock = self.deferreturnblock
			saved_debug_current_scope = self.DebugInfo.current_scope
			saved_defers = self.defers
			saved_func_symtab = self.func_symtab
			saved_type_symtab = self.type_symtab
			self._codegen_FunctionAST(node, ir.IRBuilder(), False)
			#restore the saved blocks
			self.builder = saved_builder
			self.deferreturnblock = saved_deferreturnblock
			self.DebugInfo.current_scope = saved_debug_current_scope
			self.defers = saved_defers
			#self.func_symtab = saved_func_symtab
			self.type_symtab = saved_type_symtab
			#because this was generated inside a function(cont == False) return a reference as it is either part of an assignement or a call
			return self.module.get_global(node.proto.name)

		# Reset the symbol table now. It should work well for lambdas too. Prototype generation will pre-populate it with
		# function arguments.
		self.defers = []
		self.func_symtab = {}
		self.type_symtab = {}
		self.type_symtab.update(self.global_type_symtab)

		# Create the function skeleton from the prototype.
		#try:
		#	a = self.module.get_global(node.proto.name)
		#	print("function {0} overloaded".format(node.proto.name))
		#except:
		#	print("nooverload")
		func = ir.Function(self.module, node.proto.irtype, node.proto.name) #self._codegen(node.proto, builder)
		if node.linkage != "external":
			toset = self.module.get_global(node.proto.name)
			toset.linkage = node.linkage

		if not cont:
			toset = self.module.get_global(node.proto.name)
			toset.linkage = "internal"
		# Create the entry BB in the function and set the builder to it.
		bb_entry = func.append_basic_block('entry')
		if self.isdebug:
			func.set_metadata("dbg", self.DebugInfo.function(self.module, node.proto.name, node.sourceLocation.line, self.DebugInfo.function_type(self.module, node.proto.irtype)))
		#FIXME this will be the second block
		self.deferreturnblock = func.append_basic_block('deferreturnblock' + node.proto.name)
		self.builder.position_at_start(bb_entry)
		self.builder.debug_metadata = self.DebugInfo.location(self.module)

		# Add all arguments to the symbol table and create their allocas
		for i, arg in enumerate(func.args):
			t = arg.type
			self.builder.debug_metadata = self.DebugInfo.location(self.module)
			alloca = self.builder.alloca(t, name=node.proto.argnames[i])
			#FIXME local_variable and variable_type
			self._debug_emit_alloca(node.proto.argnames[i], alloca, self.builder)
			self.builder.store(arg, alloca)
			self.func_symtab[node.proto.argnames[i]] = alloca
			self.type_symtab[node.proto.argnames[i]] = node.proto.argtypes[i]

		if node.ret.type != 'none':
			alloca = self.builder.alloca(node.proto.returntype, name="returnvalue")
			self.func_symtab["returnvalue"] = alloca
			self.type_symtab["returnvalue"] = node.ret
		assert(isinstance(node.body, AST.CompoundStatementAST))
		self._codegen_CompoundStatementAST(node.body, self.builder, True)

		if not self.builder.block.is_terminated:
			self.builder.branch(self.deferreturnblock)
		self.builder.position_at_start(self.deferreturnblock)

		for i in self.defers:
			self.builder.debug_metadata = self.DebugInfo.location(self.module)
			self._codegen(i.body, self.builder)

		if node.ret.type != 'none':
			self.builder.ret(self.builder.load(self.func_symtab["returnvalue"]))
		else:
			self.builder.ret_void()
		self.builder = None
		return func
