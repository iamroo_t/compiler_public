from ctypes import * #CFUNCTYPE, c_double

import llvmlite.binding as llvm # type: ignore
import llvmlite.ir as ir # type: ignore

import CodeGen
import Transpile
import Parser
import TypeInference
import Type
import AST
import Debug

import time
from copy import deepcopy, copy
import pickle

class Evaluator(object):
	"""Evaluator for expressions.

	Once an object is created, calls to evaluate() add new expressions to the
	module. Definitions (including externs) are only added into the IR - no
	JIT compilation occurs. When a toplevel expression is evaluated, the whole
	module is JITed and the result of the expression is returned.
	"""
	comptime_function_counter = 0

	def __init__(self, includePaths, toLinkPaths):
		llvm.initialize()
		llvm.initialize_native_target()
		llvm.initialize_native_asmprinter()
		llvm.initialize_native_asmparser()
		#llvm.initialize_all_targets()
		#llvm.initialize_all_asmprinters()

		self.llvmcodegen = CodeGen.LLVMCodeGenerator()
		self.llvmcodegen.evaluator = self
		self.llvmcodegen.module.triple = llvm.get_default_triple()
		self.ccodegen = Transpile.CCodeGenerator()
		self.parser = Parser.Parser(includePaths)
		self.typeInferer = TypeInference.TypeInferer()

		self.target = llvm.Target.from_triple(llvm.get_process_triple())
		self.toLinkPaths = toLinkPaths

	@classmethod
	def getcomptimefuncname(klass):
		ret = "_comptime" + str(klass.comptime_function_counter)
		klass.comptime_function_counter += 1
		return ret

			#node was codestr
	def evaluate(self, node, builder, optimize=False):
		"""Evaluate code in codestr.

		Returns None for definitions and externs, and the evaluated expression
		value for toplevel expressions.
		"""
		#ast = self.parser.parse_toplevel(codestr)
		#self.llvmcodegen.generate_code(ast)

		#self.llvmcodegen = CodeGen.LLVMCodeGenerator()
		#codegen.module = deepcopy(self.llvmcodegen.before_function_codegen_module)

		proto = AST.PrototypeAST(self.getcomptimefuncname(), [], [], node.irtype, False)
		proto.irtype = ir.FunctionType(node.irtype, [])

		finalnode = AST.ReturnExprAST(node)
		finalnode.irtype = node.irtype

		anonfunctorun = AST.FunctionAST(proto, finalnode, proto.irtype)
		anonfunctorun.irtype = proto.irtype
		anonfunctorun.proto.irtype = proto.irtype
		anonfunctorun.ret = proto.irtype

		self.llvmcodegen._codegen(anonfunctorun, builder)

		llvm.check_jit_execution()
		llvmmod = llvm.parse_assembly(str(self.llvmcodegen.module), llvm.get_global_context())

		if optimize:
			pmb = llvm.create_pass_manager_builder()
			pmb.opt_level = 2
			pm = llvm.create_module_pass_manager()
			pmb.populate(pm)
			pm.run(llvmmod)

		# Create a MCJIT execution engine to JIT-compile the module. Note that
		# ee takes ownership of target_machine, so it has to be recreated anew
		# each time we call create_mcjit_compiler.
		target_machine = self.target.create_target_machine()
		with llvm.create_mcjit_compiler(llvmmod, target_machine) as ee:
			ee.finalize_object()

			#@FIXME CFUNC return type should be ir equivalent
			fptr = CFUNCTYPE(c_int32)(ee.get_function_address(anonfunctorun.proto.name))
			result = ir.Constant(node.irtype, fptr())

			return result

	def compile_evaluated_to_object_code(self, codestr, optimize = True, llvmdump = False):
		"""Compile previously evaluated code into an object file.

		The object file is created for the native target, and its contents are
		returned as a bytes object.
		"""
		# We use the small code model here, rather than the default one
		# `jitdefault`.
		#
		# The reason is that only ELF format is supported under the `jitdefault`
		# code model on Windows. However, COFF is commonly used by compilers on
		# Windows.
		#
		# Please refer to https://github.com/numba/llvmlite/issues/181
		# for more information about this issue.
		target_machine = self.target.create_target_machine(codemodel='small')

		# Convert LLVM IR into in-memory representation
		llvmmod = llvm.parse_assembly(str(self.llvmcodegen.module))
		return target_machine.emit_object(llvmmod)


	def compile_to_object_code(self, codestr, optimize = 0, llvmdump = False, astdump = False, asmdump = False, filename=""):
		"""Compile code into an object file.

		The object file is created for the native target, and its contents are
		returned as a bytes object.
		"""
		target_machine = self.target.create_target_machine(opt=optimize)
		target_machine.set_asm_verbosity(self.llvmcodegen.isdebug)

		asts = []
		ast = self.parser.parse_toplevel()

		start_time = time.perf_counter()
		while ast != None:
			asts.append(ast)
			ast = self.parser.parse_toplevel()
		print("parsing and lexing took {0} seconds".format(time.perf_counter() - start_time))

		start_time = time.perf_counter()
		for ast in asts:
			self.typeInferer.inferType(ast)
		print("type inference took {0} seconds".format(time.perf_counter() - start_time))

		if astdump:
			for ast in asts:
				print(ast.dump())

		#for ast in asts:
			'''if isinstance(ast, AST.PrototypeAST):
				self.llvmcodegen._codegen_PrototypeAST(ast, None)'''
		'''	if isinstance(ast, AST.FunctionAST):
				self.llvmcodegen._codegen_PrototypeAST(ast.proto, None, True)'''

		self.llvmcodegen.targetinfo = target_machine.target_data

		start_time = time.perf_counter()
		for ast in asts:
			self.llvmcodegen.generate_code(ast)

		if self.llvmcodegen.isdebug:
			Debug.DebugInfo.dwarf_and_debug_info(self.llvmcodegen.module)

		# Convert LLVM IR into in-memory representation
		llvmmod = llvm.parse_assembly(str(self.llvmcodegen.module))
		llvmmod.data_layout = str(target_machine.target_data)
		llvmmod.name = filename

		print("LLVM codegen took {0} seconds".format(time.perf_counter() - start_time))

		#FIXME not working tmp fix is in alang.py
		#for lib in self.toLinkPaths:
		#	llvm.load_library_permanently(lib)

		if llvmdump and not optimize:
			return(str(llvmmod))

		if asmdump and not optimize:
			return(str(target_machine.emit_assembly(llvmmod)))

		# Optimize the module
		if optimize:
			start_time = time.perf_counter()
			pmb = llvm.create_pass_manager_builder()
			pmb.opt_level = optimize
			pmb.loop_vectorize = True
			pmb.disable_unroll_loops = False
			pmb.slp_vectorize = True

			pm = llvm.create_module_pass_manager()
			pmb.populate(pm)
			pm.run(llvmmod)

			print("LLVM optimizations took {0} seconds".format(time.perf_counter() - start_time))

			if llvmdump:
				return(str(llvmmod))

			if asmdump:
				return(str(target_machine.emit_assembly(llvmmod)))

		return target_machine.emit_object(llvmmod)

	def compile_to_c_code(self, codestr, filename=""):
		asts = []
		ast = self.parser.parse_toplevel()

		start_time = time.perf_counter()
		while ast != None:
			asts.append(ast)
			ast = self.parser.parse_toplevel()
		print("parsing and lexing took {0} seconds".format(time.perf_counter() - start_time))

		start_time = time.perf_counter()
		for ast in asts:
			self.typeInferer.inferType(ast)
		print("type inference took {0} seconds".format(time.perf_counter() - start_time))

		program = """#include <stdint.h>
#include <stdbool.h>
typedef struct alang_any {} alang_any;"""
		for ast in asts:
			'''if isinstance(ast, AST.PrototypeAST):
				self.ccodegen._codegen_PrototypeAST(ast, None)'''
			if isinstance(ast, AST.FunctionAST):
				program += self.ccodegen._codegen_PrototypeAST(ast.proto, True)

		start_time = time.perf_counter()
		for ast in asts:
			program += self.ccodegen.generate_code(ast)

		if self.ccodegen.isdebug:
			Debug.DebugInfo.dwarf_and_debug_info(self.ccodegen.module)

		print(program)

		print("C codegen took {0} seconds".format(time.perf_counter() - start_time))

		return program
