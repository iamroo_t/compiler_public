class Type(object):
	def __init__(self, type, indirection, qualifiers, isarray, arraysize):
		self.type = type
		self.indirection = indirection
		self.qualifiers = qualifiers
		self.isarray = isarray
		self.arraysize = arraysize

	def dump(self, indent=0):
		r = ""
		for i in self.qualifiers:
			r += i + " "
		return '{0}{1}{2} is array:{3} arraysize:{4}'.format(
			' type:', r, str(self.type) + " " + "*" * self.indirection, self.isarray, self.arraysize)
